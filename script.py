from selenium import webdriver
from time import sleep
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from collections import Counter
import re
import sqlite3

driver = webdriver.Chrome(executable_path='./chromedriver')
options = Options()
options.add_argument("start-maximized")
options.add_argument("disable-infobars")
options.add_argument("--disable-popup-blocking")

main_urls = "https://www.nderf.org/NDERF/NDE_Archives/archives_main.htm"

def get_main_urls():
    driver.get(main_urls)
    url = [ x.get_attribute('href') for x in driver.find_elements_by_tag_name('a')]
    return url[5:]

def save_on_db(link):
    conn = sqlite3.connect("urls.db")
    cursor = conn.cursor()
    cursor.execute("insert into url(url) values (?)", (link,))
    conn.commit()
    conn.close()

def get_links_from_db():
    conn = sqlite3.connect("urls.db")
    cursor = conn.cursor()
    return cursor.execute("select * from url").fetchall()



def get_text():
    for i in get_main_urls():

        counter = 1
        driver.get(i)
        driver.implicitly_wait(4)
        links = driver.find_elements_by_xpath('//a[contains(@href, "https://www.nderf.org/Experiences/")]')

        for j in links:
            print(j.get_attribute("href"))
            driver.implicitly_wait(4)

            save_on_db(j.get_attribute("href"))


def save_file(text, counter):
    open('files/{}.txt'.format(counter), 'w').write(text)

def save_file_erro(links):
    open('files/erro_links.txt', '+a').write(links)

def get_text_no_red_text():
    j = 0
    for i in get_links_from_db():
        driver.get(i[0])
        driver.implicitly_wait(3)
        text = driver.find_element_by_css_selector('body').text

        try:
            text_data = text[text.index('Experience Description'):text.index('Background Information')]
            save_file(text_data.replace('Experience Description', ''), j)
            j+=1
        except:
            save_file_erro(i[0])


print(get_text_no_red_text())
