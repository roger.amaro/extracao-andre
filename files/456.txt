
As I began to hemorrhage, my husband carried me to the car and put me in the back of our Pinto wagon. As he was speeding to the hospital, I could feel the blood gush out with every heartbeat. I was lying in a puddle of my own blood that extended from my toes all the way up to the back of my head. I knew it wouldn't take long before I would bleed out at this rate. My heart was pumping fast with anxiety and I was praying fervently for God to allow me to live, at least long enough to raise my children.

Suddenly, I no longer heard the noise from the car, or felt the bumpy road. Everything became quiet and peaceful. I felt a warm, large hand gently caress my hair on the top of my head and a sweet voice telepathically told me, 'It's okay. It's not your time; I have more for you to do.' At that point, my heart slowed down and I knew, for certain, that I would live through this no matter what.

Once we reached the hospital, the doctor had to do emergency surgery and had no time to get an anesthesiologist. He told me if I needed to scream, that it was okay. But even though it was painful, I was surprised that it wasn't as bad as all the nurses and doctor thought it would be.
