

I was 6 weeks pregnant and went to the Clinic for an abortion. The doctor gave me an overdose of lidocaine to the womb and I passed out immediately. I awoke to a panicking nurse standing above me with paddles and in a panic. What happened to me during that time forever changed me.

I was taken to a beautiful meadow with yellow flowers everywhere. I’m not sure what kind of flowers they were. For a while, I was just ‘walking’ around in this meadow taking in all of the feelings. I say 'walking' because I didn’t really have a body. I was just sort of floating around.

I remember feeling the best I’ve ever felt in my entire life. I know that I will never feel as good as I felt on that day and that place. For the first time ever, I felt whole, complete and like I belonged. It’s so overwhelming and hard to describe how great I felt. I remember thinking ‘Am I dead?'

After awhile, I noticed to my left a line of evergreen trees. I knew not to go to the trees because once I passed, I would be in another realm. I wasn’t ready for that yet.

Then I was in complete awe and happiness because I saw my deceased Grandmother above the trees. I was in shock because she looked the same age as when she died. She never said anything, but we were happy to reunite. She had a glow and I could only see her from her shoulders to her head. I remember thinking, 'Thank God, she is o.k.! I’m in Heaven and must tell my family.'

Then Jesus appeared beside Grandma. Nothing was said or implied, but He began to fade and change. The next thing I knew, was that the nurse standing over me replaced Jesus. I realized that I was back.


