
I knew I was dying and there was nothing to stop it. My lungs were filling with blood and I knew I was drowning. I began reciting Psalms, with no clue how I knew the verses I was reciting. Then I said the Lord's Prayer and asked forgiveness. I asked my dad to help me because more than anything I just wanted to see my dad.

The next thing I knew he was there holding me and telling me everything was all right. I was amazed that he looked about 25 years old. Then I realized I could breathe and wasn't in the parking lot anymore. There were other people around and they, like my father, were all young. I knew these were all people who knew and loved me in life but had moved on. Then my dad said he wanted me to meet someone. He introduced a man named David to me. I don't know how I knew, but he was King David from the Bible. David asked if I would pray with him and we recited the Lord's Prayer together. After that, we all talked for a while. I noticed a man standing nearby and he was watching me the whole time. I asked if that was Jesus and my dad said, 'Yes it is.' I asked why Jesus wasn't talking to me. My dad said he was making a decision whether to take me then or send me back. I asked my dad what he thought and he said he didn't know.

After that, many people came and spoke with me. Both, dad and David stayed with me the whole time. It seemed like I was there for days and spoke with a hundred or more people. Some of these people I had only seen as a baby. I didn't recognize them but somehow knew whom they were when they spoke to me.

Some, I knew the second I saw them. After a time my dad said, 'You have to go back now,' I knew better than to protest even though with all my heart I wanted to stay.

I never knew how loved I had been, and still am, until this happened. I woke up in the ambulance briefly and died two more times on the operating table. I only left my body once. I will have to describe that as a separate experience.
