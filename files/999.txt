 7255
I was living in Iowa. It felt like my stomach had exploded. The next thing I knew when I woke up, I was in an ambulance. I was rushed into surgery. While I was in recovery, my parents said everything went OK. As I sat up a little to talk to them, everything burst wide open again. I was rushed into emergency surgery again.

As I was being operated on, I remember looking down at the surgeons working on me. I tried to tell my parents everything was OK. After a few seconds or minutes, I felt a warm, glowing light; it felt so warm and so good. I met a person who said she would take care of me. I asked, 'Who are you?' Her reply was, 'I am your grandmother's mother.' I had never met this person. After a few seconds or minutes, I told her I was starting to get cold. Her reply was, 'You will need to go back now.' I did not want to go back. She said, 'You must; it is not time yet.'

The next thing I remember was waking up in the recovery room with my parents.
