


A Flight Without Wings: My Experience with Heaven   Check it out at:  Click Here







I don't remember anything from right before the accident or the accident itself. My memory came back shortly after arriving at the hospital. I had a very keen sense of what was going on around me, my journey into heaven, and meeting up with my deceased father. My father told me I had to go back. We both turned away from each other.

I opened my eyes and woke up to see people in the hospital. I had been in surgery for 5 hours. After that, I had spotty memory for the next 3 months or so. I remain in the dark about the accident, the airlift out of Mexico, and the second surgery in the U.S. to reconstruct my face. I don't remember very much about the months recuperating at home.

The memory of my 'heaven' experience remains vivid, but without benefit of sense of time. The aftereffects are extensive and difficult to explain. But they include heightened sense of intuition by feeling other people's pain, compassion, and their joy. The benefits continue to present themselves in sometimes obscure ways, which are challenging to manage.
