

On 28.01.2016 at 11:50 a.m., I had a c-section. When I was brought back to my room, I got up at 5:00 p.m. and went to the bathroom to smoke. My mother was with me all the time.

When I lit the cigarette, my arms felt numb. My tongue felt numb and my head was spinning. I told my mother that I was going to faint and that she should call for a physician. As soon as my eyes closed, I found myself in another place.

This place was different. Everywhere there was lush green nature. It was a very peaceful place. I was very happy. There were green meadows and a train rail. In a distance, a train came very fast to where I was standing.

The train stopped to pick me up. I wanted to get in the train and depart. In that moment, I looked back and saw my mother holding my newborn son. My mother told me not to take the train and leave my son without a mother. My mother said, 'No one will be as good to him as you can be. He is just an infant.'

I felt very light-weight as a wind dragged me softly toward the train. I was very happy and didn't wanted to leave with the train. My mother ran toward me and stood in front of me to stop me. She said 'Don't go!'

I changed my mind and did not want to leave with the train. While I wanted to take my newborn son in my arms, the train took of without me very fast.

In that moment, I woke up. Two nurses and my mother were with me. I asked them where I was now because I was somewhere else. I then said, 'I was probably dreaming.' The nurse then told me that my eye pupils were very small, hardly visible and my lips were purple for several minutes. She said that the nurses could not measure my pulse for several minutes. I think I died and came back.

