

I was feeling apprehensive before the operation. I had had three previous c-sections in the same room with the same doctor at the same birthing center, but felt an impending sense of dread for this particular one. I figured it must have been just nerves.

They got me all prepped for surgery. I walked into the operating room and climbed up on the table. They started by getting the IVs put in. Then the doctor came in and said, 'We start in a few minutes.' (Once the medications take effect, they have to get the baby out as quickly as possible so the anesthesia medications will have as little effect on the baby as possible.)

The doctor nodded to the nurse. She injected medicine into my IV-line and the rest of them were continuing to prepare the room for surgery. Then I was out and it was all black.

The next moment of consciousness, I was looking at the scene from above. I was not above my body in the same room, but I was way up above.

I was perfectly fine, in no pain or anything whatsoever. Whatever it was my body was going through, ~I~ was not. I was standing, couldn’t feel the ground but had the sense of standing firmly. I was near a railing, or small wall that was surrounding a shallow pool. I was speaking with and instructing three or four other women who were to my right. The women were holding something liek notebooks or clipboards and were taking notes on what I was explaining to them. We were all wearing soft white-colored robes or long coats of some sort. Mine was a bit different than what their robes were like.

When we looked into the shallow pool that we were standing by, we didn’t see water. We saw the operating room. It was as if someone had cut the entire ceiling of the room off and place a short, thick, enormous tube on the top instead. I would lean over the railing and point to things that the doctor or nurses were doing and explain it to the young women next to me. It looked like they took my organs out one by one, placed them on my body with a thick rope or string still attached to them. As time went on, we could see the doctor getting very upset about something. He was yelling at the nurses, as he felt many emotions as he was on the verge of panic setting in. I explained other things to the women. They asked questions and conversed with me. I’d give anything to remember what else we were talking about, but I don’t remember anything else discussed except when I was explaining what the doctor was doing.

As the doctor and nurses seemed to be getting more flustered and upset, there were other people passing by on the right side of us. Some of these people were soon calling to and pulling aside the young women who were with me. I was watching them and still talking to the one or two young women closest to me. My attention was soon turned towards the shallow pool. For some reason, the pool seemed to be getting fuzzy, so I leaned closer. The images below were also getting closer. I felt myself being pulled towards the pool, head first. And then I was falling forward, while colors, sounds, and shapes were being blasted at me. They were rolling over me and swallowing me.

Then, I was back in my body. I was in EXCRUCIATING pain, as all women who have had c-sections feel. I was being wheeled out of the operating room. I do remember thinking, 'Man, where was that place I was just in? Can’t I go back there? What did those women say again? Just let me go back there' Later, my husband said that when the doctor came out of surgery, he was drenched in sweat, taking heavy breaths and seemed exhausted. The doctor repeatedly told my husband that he does not advise any more children because my uterus can’t take it anymore.

Upon waking, I didn’t remember that I had experienced something unusual because it was just me continuing on from where I had been. Thinking back on it later, the experience never seemed odd or strange. I thought, 'Oh, this must have been just a dream I had had', but it was just my day naturally continuing on.

My first c-section was an emergency, and here in Japan, once a c-section is performed, then any birth after also needs to be a c-section. My second delivery was horrible. The doctor tried to do it while I was awake by using an epidural. When doctor went to stab me, I could feel it. He laughed at me and said it was just nerves, but I could feel it all. They then put me out, because I was freaking out. I still woke up repeatedly during the surgery. For the third delivery, they gave me more medication than the first two. It was awesome and practically pain free. With the first three deliveries, I had never experienced anything like this last experience.

Upon waking, I didn’t remember that I had experienced something unusual because it was just me continuing on from where I had been. Thinking back on it later, the experience never seemed odd or strange. I thought, 'Oh, this must have been just a dream I had had', but it was just my day naturally continuing on. I continue to wonder, wish and pray for a dream or anything to hear what we were talking about again.

I do know the afterlife is real. I do know our actions here determine what afterlife experience we will get. And I pray for love and peace for all of you. No matter how hard life gets, I pray that you will all keep on going and hoping for better.


