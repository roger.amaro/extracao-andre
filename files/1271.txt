
I was climbing a mountain which was so steep that I was climbing it on my knees and hands. There was no noise. There was a very bright light at the top of the mountain, and a very strange and frightening darkness coming up the mountain, which looked very menacing and was so dark that you could not see anything.

I only had one thought in my mind. Climb to the top, to the light before the darkness comes. If the darkness comes and catches me up, I will not be able to come back.

I felt the presence of several entities who were watching me and encouraging me. I also felt the presence of entities who were coming from the darkness trying to catch me.

It was an extremely terrifying event.

I had flashbacks of this event for years after it happened although I forgot all about it for a while.

It stopped when a man had thrown me on the floor of the hospital, was sitting over me, was slapping me and was shouting very loudly, 'Wake up, Wake up, and Wake up!' He stopped slapping and shouting at me when I reacted although I did not see him. I heard him, felt his presence on top of me, and lost consciousness again. It took me a week to fully come back to consciousness.
