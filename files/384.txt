
I was on a camping trip in the Spring of 1982 with my friend Dave. We went on a hike and ended up following a river. There was still some snow and ice around and we thought of going swimming, but we couldn't even tolerate just standing ankle deep in the water. It was just too cold.

Dave was walking along the river, but I had jumped on several boulders to end up standing on a huge boulder about 20 feet over the water. Dave jokingly said, 'Jump!'. So I did, just for the hell of it. Half way down I realized that I didn't know how deep the water was or what the bottom of the river looked like. It was Spring run-off from the melting snows and the water was moving fast. Therefore, I flattened out to land flat on my back. As soon as I hit, the cold took my breath away. I was being pulled down the river and when I surfaced, I could see Dave frantically running down the banks screaming a me.

I then got caught in an undertow and I could feel myself being bashed along the bottom and having my body being thrown into boulders. I was aware of the numbing cold and pain all over my body from being tumbled across the bottom. I was out of breath and tried to reach the surface to get air, but couldn't make it. I felt my lungs burning for the want of air. I knew I was drowning and I was thinking that this was 'it'. I pictured my parents reading the headlines of my drowning. Then it got dark as I got caught in a swirl of water under a huge boulder. I remember the slime on the bottom of the boulder, while I searched with my hands for the way out from underneath it.

It felt like I hadn't breathed in hours. I could feel my chest pumping in and out, my lungs bursting in my chest. I could feel the darkness and the pain in every part of my body. Then everything changed.

I could see bright light all around me, but I couldn't tell from where it was coming. It seemed like the tumbling stopped, because I could focus straight ahead. I felt a warmth enveloped me. I didn't feel the cold, any pain, nor did I feel the need to breathe. I felt safe, warm and calm. It was a calm I had never felt before. I could see figures of people in the bright light, but I couldn't see who they were. I felt like I was travelling towards them. I knew I was dying and I remember thinking that this is not too bad. It was actually pleasant and I was looking forward to it.

The next thing I knew was I popped out of the water and Dave was frantically trying to get to me. I travelled over a hundred yards down stream. When I came out, I was covered in bruises all over my body, but had no broken bones.

