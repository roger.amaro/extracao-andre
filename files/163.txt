

After separating from my husband and experiencing a reoccurrence of my painful, undiagnosed illness, I fell into a deep depression. I took a fatal amount of my medication and within half an hour, I was gone.

I slipped in and out of consciousness and was no longer in pain; in fact, I felt a sense of relief. I was pulled from my body, not swiftly, but rather slowly and entered what can best be described as cold, heavy darkness. I was drifting there in this indefinable place for an unknown length of time, aware that I was somewhere else, no longer bound to my body. There was no fear, but a sense of calm. The darkness felt like a pervading weight holding me there and I wondered if this was where I was meant to stay and began to feel restless, as though I didn’t intend to linger, this was not where I belonged.

Then a flood of light enveloped me and I had the sense of being pulled up and transitioning somewhere else. It was very bright, and I entered a field. Vivid green grass and blue sky, all of it rendered with such ethereal brightness, yet it was the feeling while there that is indescribable. Pure love, contentment and complete acceptance. This was home, I wanted to stay. I was at peace. But for whatever reason, I was sent back, it was not my time.

I descended to the limited perspective of my body and overheard all that happened when I was found clinically dead; cold and not breathing. When I was resuscitated, I recall a profound sense of loss. It took days before I felt like I’d completely returned and adjusted to what feels like another life. I can’t articulate how I am now compared to how I was before I died. While I still experience bouts of depression due to witnessing much suffering that happens in this world, I recall the love I felt and I have no fear or anxiety. The unknown illness I had for six years also completely disappeared, a miracle, much like my return, as I had no side effects or organ damage despite the large amount of medication I took.


