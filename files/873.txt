
I went into hospital for surgery for chronic amygdalitis. I was in the operating theatre and talking to the nurses and the doctor, they gave me an injection in the arm, of something, and I slept. [Editor’s Note: Amygdalitis is an acute inflammation of the emotional centre of the brain.]

After the surgery but before waking, I walked into a dark tunnel. The tunnel was made of earth. I was wearing my white frilly dress and girl’s tights. I was braiding my hair into two braids, yet never saw my face. Suddenly, I don't know how, there appeared in front of me a tall slim man with a white trailing tunic that reached the floor. I never saw his feet. His hair was shiny, limp, and fell to his shoulders. I never saw his face because we both were looking in front of us. As he took me by the hand, I noticed that the skin on his hands was very white.

Just then, I woke up. The nurse was injecting something into my veins to stabilize my body. I was cold, so cold. They wrapped me in blankets to warm me. I chatted with my parents and I slept.

The anesthetic had harmed me, although I really don't know what happened. In all the years, I have never been able to forget this experience. I remember perfectly every day as if it had happened today.

As the years pass, strange things happen. I see people, but they don't harm me or frighten me. I don't know why. I pray for their blessed souls.
