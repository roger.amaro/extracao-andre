

First I said, 'Salad.' Then I went blind, and then deaf.

My consciousness went to a comforting gray, grave-like box. I was not afraid.

When I came to, I had a strong message, 'You have much work to do.' It was the first thing I said. I woke up 2 days later.


