
When I was about five, I drowned in the open sea. A wave overturned the boat where I was sitting with some relatives. That morning, I was the only girl staying on the bottom of the sea, accompanied by a cone of light. Despite the water that filled my lungs, the cone of light made me breathe and taught how to swim, in a very fast and telepathic way. So it saved my life.

All this might seem like an altered memory of a little girl, but believe me, it has transformed my life. In the following period, I began to be a special little girl. I saw things, beings and people that others did not see, but mostly I had premonitory dreams, of which my parents were afraid. In fact, everything I told them when I woke up in the morning, usually happened only a few minutes later.

At the age of eighteen years old, is the epilogue. At that time, I was undergoing a minor turbinate surgery. This surgery was supposed to last up to thirty minutes. Instead, I came out of the operating room after five hours because I was dying from internal bleeding.

I found myself out of my body as if to watch over it. I found myself observing it peacefully. I was at the back of my head, at the same height as the operating table. I was watching my body as a mother looks on a little son in the crib. Yes! I loved that body, even though I had a hard time recognizing it was mine.

A little later, a blinding light showed up to my right. Very firmly and with authority, it ordered me not to turn around, without words, as I did not need words to hear it.

The light ordered me not to turn around because, 'It was not a place that I had to see.' That massive being rested his hand on my physical face that was immobilized by anesthesia, and moved it. The blood came out from my mouth, unlocking my throat. I was saved.

I started circling in the air, but only to pass through a wall. I found myself in the next room, next to a nun who was sterilizing some surgical instruments. I scared her, distracting her from what she was doing. Was I a ghost? The nun glanced up and saw what was happening in the big glass in front of her, in which she could see me, that is, my body. I remember her screams at seeing me losing blood and the immediate action taken by the medical staff.

I suddenly felt that I was being sucked into a place that I knew was my brain! It was dark, and I stayed there for a few seconds waiting for something to happen. Then I woke up. I had several pieces of evidence as to what had happened to me. A week later, I went back to school, and one of my classmates told me that she felt that I needed to read a shocking book and lent it to me. It was called 'Life after Life'.

That was not the only thing that happened to me. From that day until now, the Being who saved my life has always made sure he is standing next to me, by concretely solving everything. Once he spoke to me for a few seconds and hugged me. I could feel it as someone that is trying to tune a radio, and a lot more that is miraculously absurd.

Anyway, what I want to say is that everything ensued in my life, especially my quick change, has been oriented toward the Good and to convince people that 'GOD EXISTS'.
