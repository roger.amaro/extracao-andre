
It was 11 pm, I was returning home from university and as a joke I sat on the hood of the car. My cousin was joking around and started to drive the car with me sitting on top. I was frightened and didn’t feel safe, so I jumped off the car. I hit the ground, head first. I went unconscious.

I went to a different place. It was quiet and pleasant. I felt very light. The feeling was so great I can’t describe it. I could taste sweetness inside me; everything was beautiful. I was in that moment for around 30 minutes. In that moment, I knew that whatever I wanted I could get. For a second I thought ‘who am I?’ I saw my whole life flash before me like a movie. I could go to any time in my life's review. It was amazing. I felt like my intelligence and thoughts were beyond normal. I did not feel like I didn’t understand, it was a great feeling and I wished I could stay here and not leave. It was having the thoughts that I was wondering, ‘What can I do?’ as I could do anything.

But then everything went dark and I could feel the pain in my back and the heaviness in my body. I then came back to consciousness and was in the hospital with my friends around me.
