

I had an operation many years ago. While I was being anesthetized and was supposed to breathe through a mask, I felt that I left my body. I rushed at a terrible speed up through a lot of white ceilings. I ducked my head because I thought I was going to bump into these ceilings. It tickled my stomach as it was so intense.

Suddenly, I was still. I could see the planet earth far below with a blue corona. I could look straight down into the operating room. One person was standing on the right and two were on the left. I wasn't interested in that. I was much more surprised and amazed that I was in outer space. I thought: It was supposed to be cold there with no oxygen. I didn't notice anything else.

I looked at my arm and was really amazed. It was made of light with a little corona around it. My whole body consisted of white light. Everything consisted of a white light. It was a body and then still it wasn't a body. It isn't easy to explain. I was thinking how beautiful it was. I wasn't afraid, but was very, very amazed. I looked out into space. It was dark, and I saw a white light getting close. It was going so fast that I don't know if I was going toward the light or the light was coming toward me. It was going really fast.

My experience ended here when I came into the light. Later I thought it might be a dream that felt very alive. But many years later, I went flying for the first time. We went through a lot of cloud layers, and my experience with 'white ceilings' came to mind. On TV, I saw the planet earth from the outside and I saw this blue corona. At that time I didn't think that's how it was. It's been a very long time since then. I got interested and started reading whatever I could find out about it. To my surprise, there's a boy who talks about the same thing. He came out into outer space. I think about my experience quite a bit because it was wondrous and alive. So many years have passed since then, of course, but it still is alive for me. I think about how there must be a God.


