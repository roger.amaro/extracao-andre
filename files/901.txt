
On the three occasions I was very tired or severely unwell prior to the experience. Afterwards, I was sad to have left the place and had to make efforts to thank the people who saved my life; feeling I wish they didn't interfere. My personal life was great; it had nothing to do with my earthly life.
