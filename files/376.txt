
I saw and heard my husband cry and did not understand the reason. For me, everything was going to pass and I was well. I said to him: 'Manuel, I'm well! It’s over!' He didn’t hear me. I insisted. He went to grab the device to measure the blood pressure, put it on my arm and when it got the result he moaned. He tried it several times in vain because the device didn’t give any signal.

I continued to look at him and telling him that I was well. He didn’t hear me. I saw that he went to get a small mirror, which he put in front of my nose and my mouth but there was no vapor in the mirror. He tried it once and again. I continued with my attempts to speak to him, telling him that I was all right and that I didn’t feel any pain. I couldn’t explain why the mirror didn’t fog up but I worried more about calming my husband. I was well. He babbled, 'Please, Gracinia! Don’t leave me! Come back to me! I need you!' I was telling him: 'Listen to me! I am well! It’s over already!'

Suddenly, I realized that I was looking at him from a more elevated point than would be possible if I were lying in bed. He used the mirror and the tensiometer so anxiously and I didn’t understand the reason. I was telling him that I didn’t feel pain, that everything was already over but he didn’t hear me. Finally, he stopped and bent over my body to cry: ‘Gracinia! (an affectionate version of my name) Don’t leave me, please! What is going to be of our son! Oh, my God!' (contrary to me, at that time my husband was a practicing Catholic in spite of some doubts, and he continued to be it for many years afterwards).

At that moment, I realized that something absurd was occurring. I was aware that I was watching the scene from close to the ceiling. I felt unsettled. It was strange. How could I see my husband crying and bent over my body? I watched attentively to have the certainty that it was me. It was me! How could that be? What was going on?

I was not scared. I was intrigued. I tried to find an explanation but I couldn’t. I looked around stunned.

I think that I stopped listening to my husband, although I saw him leaning over my body to cry.

I looked carefully around from the ceiling. I saw the ceiling lamp and the friezes of the closet doors. I was now positioned close to the ceiling and was intrigued by this situation. To my right was the closet with its three doors. I could see the surface of the top part of the door. It was full of dust and I remember thinking 'Oh, I forgot to clean the dust here!' It was then that I saw a sheet of blue paper with twenty-five lines, covered with dust. It was a document that I had searched for and couldn’t find. I thought, 'It’s here. I looked around for it so much and it is here and covered in dust. I’ll have to clean more carefully.'

I was aware that this was not a dream. Below, my husband shook my body and I felt sorry for him. I did not think that I was dead. I was not dead because I could watch the scene from a physically impossible point of view. I was unable to understand anything. I felt unsettled. I looked at the wall on my back and saw the clock. When I tried to see the time, I felt 'sucked' and left that space.

In the following instant, I was in a place and in total darkness. I felt a lot of fear and was disoriented. I extended my arms trying to reach a wall or a piece of furniture. I couldn’t reach anything. I remember I was going forward with my extended arms and rolling in all directions looking for a point of reference. There was Nothing and I was terrified. Where was I? What was I doing here?

I didn’t call God or the saints. To me, they did not exist. I didn’t call for anyone.

I decided to walk in one direction, at least to try walking. I extended my arms towards the front and I moved.

It was then that I heard a voice: 'Don’t be afraid! We are here to help you!' I extended my arms in the direction of the voice but all I found a void.

Another voice, and still another, said the same to me. I cannot tell how many of them there were. I felt their 'presence' but I couldn’t touch them.

At a certain point, I realized that I didn’t hear them with my ears but with my thoughts. How was that possible?

I realized that I didn’t have an option. I was in the darkness, not knowing where, and whatever it was that was there. I had no one to ask for help. These voices seemed like people to me because I could understand what they said to me.

I was intrigued to know where I was. If I didn’t see them, was I perhaps blind?

I heard the answer within me, 'No, you’re not blind!'

I stopped trying to touch them.

I mentally accepted their help. I was scared; I can even say that I was terrified. The total darkness disoriented me.

'Come!'

'Where to?'

'Come! Follow us!'

'How? I cannot see you'

'Let it go!'

Whoever it was that communicated with me was situated to the right. I felt something like an energy, like static electricity or magnetism, which I couldn’t define. I felt also that there were other 'energies' around me.

I desisted from questioning and I let it go. I felt my body to take the position of lying face down on my belly. I felt agitated but the one who accompanied me said that I would not fall.

We move slowly in the darkness. Or so it seemed to me. Without any point of reference I was not able to know with which speed I was moving. I had the sensation of gliding.

I asked 'Where are we going?' and they answered that I should be calm and to continue.

The fear was going away.

At a certain place, a point of minuscule light appeared in that darkness. They said to me, 'Look! That is where we are going!'

I did not have any questions.

As we advanced in the direction of the light, the darkness faded.

I didn't feel like I was 'travelling' through a tunnel. I had the sensation of 'travelling' in the projected cone of light, like when you use a lantern in the dark you project a cone of light. That is how I can describe that moment.

I started to feel anxious. I wanted to be there, close to the light. To me, it was a star that would illuminate the landscape when we were closer to it.

When there was clarity around me, I tried to look at my companions. I couldn’t see them but I continued to feel them close to me.

I was calm, as if everything that happened was natural. I understood.

We came to a point where the clarity illuminated everything and I saw a landscape. But I didn’t have time to look at its details because it seemed as if we were travelling at an unthinkable speed. I saw the Earth and the Moon in the distance. I saw the Sun moving away. I was amazed.

There were colors that I cannot define because they don’t fit in the palette of colors that we have on earth. There were tones that created layers like transparent, superimposed plaques. They were beautiful and I was ecstatic. I let myself be carried through, without fear. I was amazed. We traversed tons of 'dust.' Then more and more stars appeared that moved away as we advanced. I could see the light of the stars without any difficulty. That space landscape was also very beautiful. I was like a curious traveler, observing everything despite the movement being too fast to observe details.

I remember understanding. It seemed to me that I saw the stars in parallel, infinite planes. I understood everything, so I no longer asked questions. I looked and marveled at the 'scenery'. The 'landscape' was formed by colors and the stars that we passed. I was so fascinated that I did not look at the initial light.

My 'invisible' companions continued to be with me but I had the sensation that they were slowly being left behind. That, to me, was a 'normal' fact. The initial companion who was situated to my right continued to be present. I could feel it.

I looked at the light towards which we were heading. It was so powerful as the sun but its light did not hurt. I looked at it directly but did not feel discomfort. The same happened when we passed by any star. None of these lights hurt. The colors were so beautiful and different from the ones I know. I moved but I don’t recall seeing my body. In those moments, it was not important. As strange as it may seem, the more away I was from the planet Earth the more the existence of my family ceased to matter to me. I didn’t think of my son and that was very strange.

I recall that I had a sentiment similar to one that we have when after a long absence and we come back home. I felt I was 'going back home'. I was at peace and as happy as I never had been before.

At a certain moment, the first light issued sounds which I cannot define. Sound came out in 'a wave', which I call so because it moved like the waves of the sea. It was a wave of light and energy that I don’t know how to define. I had fear but my invisible companion told me to remain calm and that nothing bad would happen to me.

I stayed watching the wave as it came nearer, waiting for what would occur. When the wave touched me, I felt it gave out love. It was a love so great that even if I added the love of my parents, my husband, my son, all the family and that which I felt for them, it could not be compared to this love more than a grain of sand to the desert. I had never felt anything like it. A new wave formed and when it came to me I felt loved in an unexplainable way.

I wished to go to the source that emanated so much love. I changed focused from whatever surrounded me to center only on that strange star. I wanted to reach it soon and with each wave that touched me, I felt ever happier as if I always was part of that love.

I was close, so I thought, when the light said mentally to me:

'Stop!'

I stopped even without wanting to.

'You have to go back!'

'Go back?'

'You have to go back. Your husband needs you and your son too!'

'No! Please! I want to stay'

'You can’t! You have to go back!'

'My husband gets another woman and my son has his father and grandparents! They don’t need me! Let me stay!'

'You have to go back! You have duties to fulfill!'

'But I want to stay! Please! I have never felt so much love in my life! Don’t send me back!'

'You have to go back! You have duties to fulfill! Your mission has not yet ended!'

'Mission? What mission? I’ve already suffered so much and I’ve never been bad. Please! Please! I want to stay here!'

'You can’t! Go back!'

In the following instant, everything vanished.

The pain came back, I breathed and was on the bed with my husband crying with joy. I was crying with sadness.

At that time, the nearest hospital was about 13 kilometers away and serviced the community so badly that anyone who had to go there was scared. I was so tired that I only wanted to rest. My husband phoned a doctor who didn’t answer. I narrated for my husband the experience I had had and we cried of the emotion. He was so moved for having me safe, and I was moved by the experience and the frustration of my return.

My husband affirmed that after I put my hand on my breast and tried to breathe, I didn't open my eyes. Then, how could I see everything that happened?

I told my husband, ' Excuse me! But I didn’t want to come back! Everyting was so pretty and there was so much love!'

When our 4 year-old son arrived from a tour with his maternal grandparents and came to embrace us, I had a sentiment of guilt for wanting to leave him an orphan of his mother. But the pain of the departure from that ambiance of love was still with me.

My husband went to see the document which I said was on top of the closet. He set up a chair to stand on and retrieved it, full of dust, just as I had seen it.

We’d never heard of anything like that.

On the morning of the next day my husband took me to a doctor of good reputation. After a long examination, he took an electrocardiogram, radiographed my whole body, made reflexive and reasoning tests. I don’t remember what else he did. Then he informed us that I had had a cardiac and respiratory arrest due to an allergic reaction to the chemical composition of aspirin, and I was very lucky to have survived. My husband asked him if we should have gone to the hospital. He said that I would have never arrived alive. He thought that I seemed to have slight injuries to the heart and lungs but that I could live many years with them. I attempted to speak about what had happened but he let me not and instead told me something like this:

'Why, Grace! You were unconscious and one step away from death! I am absolutely certain that you saw noting and heard nothing. I will give you an EEG to make sure that everything is all right.' Going back home my husband and I decided to keep secret what had occurred, for fear someone would consider us crazy. The EEG was done and was deemed to be 'normal for your age'.
