:

Hello, my name is Desmond. I am a young adult who suffers from Marfan Syndrome, which is a tissue disorder affecting the majority of my body. Info on what Marfan Syndrome in its entirety can be found at https://www.marfan.org/about/marfan

Up until I was 15 years old, I suffered from very weak tissue that connected the lens to my eye. This resulted in debilitating double vision. It wasn’t until I had two reconnective surgeries that I was able to have 20/20 vision. I previously had 20/300 and 20/400 vision.

I suffer from severe asthma and always carry a inhaler around. I also have an enlarged aorta, for which I take medication. This is why I had an aortic valve replacement surgery in 2014.

In 2013 I went in for an EKG (echocardiogram) to monitor if the aorta has grown any. After the test, my heart doctor told me that I would need an aortic valve replacement surgery since it had grown since the last test. I didn’t realize how risky of a surgery it was.

When it came time for the surgery, I was admitted to the hospital. The day before my surgery, a woman who was a social worker visited me and asked me if there was anything I wanted to do before my surgery. I laughed her off and said, 'no. Unbeknownst to me at the time that the social workers expected me to die during surgery and wanted to treat me to something as a last-minute pleasure. I just thought I was lucky.

I was scared the morning of the surgery, so I comforted myself with watching TV. At the appointed time, I was wheeled into the surgery room to receive anesthesia. I had said goodbye to my family.

As I write this, I feel sad in remembrance. I love my family very dearly.

I was unconscious until I woke up struggling to breathe during the operation. I had somehow suffered from an asthma attack during my surgery. The surgeons were unaware that this was a possibility, so they ran to grab a ventilator. I suffocated and died for a few seconds.

In the middle of suffocating, I began to feel relaxed and thought, 'I’m dying. This is it.' Suddenly, I saw myself on the operating table. Then I saw my mother crying. I said, 'I love you,' but she couldn’t hear me. I saw the surgeons frantically setup the ventilator and I saw my family in the waiting room.

The surgeons confirmed post-recovery they were doing what I saw them doing. My mother confirmed that she was crying, and my family being in the waiting room was also confirmed.

Next, I found myself in a Dark Place that NDErs call 'the void.' I call it the 'Dark Place.' My time in the Dark Place was without emotion or thoughts, yet I was sentient. There was nothing to see, feel, or know in The Dark Place. I did not experience any emotions during the experience. I lost my personality, memories, and emotions. All of a sudden, a very loud voice spoke to me out of nowhere. The male voice loudly and slowly said, 'God.' I was then immediately resuscitated and put under a medically-induced coma.

I was in a coma for two weeks. The first thing I said when I woke up was, 'I heard God!' But I do not remember saying it. I spent the next 4 months out of school to healing from the surgery. I couldn’t walk for a month and I have a huge scar on my chest.

Afterthought: I was an antitheist from ages 9 to 15. I hated the Christian God, yet I didn’t believe in his existence. It wasn’t until the months following my experiences that I believed a god existed. I didn’t believe in the Christian God until 2016 when, by religious process of elimination, I discovered that I heard the Jewish / Christian God (YHWH, Father, Jesus, Spirit). Each god in ancient and modern religion had / has a name. God said, 'God' to me as his name. God had a booming, Male voice, which is how the Bible describes his voice. I believe The Dark Place is Sheol, which is not Heaven or Hell. It is the Grave, the place of the temporary dead. Sheol is described in the Bible as darkness. I believe that since I did not stay dead and because my sin was not atoned for, I could not enter God’s presence.

The joy of knowing God exists. I believe that the naturalistic worldview is as false as it is existentially pleasing. Our lives are not terribly long and everybody will see God once we are dead.

