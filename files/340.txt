

In March 2003, I remember my son was 4 months old at the time. I had been sick during my whole pregnancy because of my gall-bladder. At the time of the pregnancy, I constantly had feelings of faintness. The doctors thought that it was some kind of disease connected with the pregnancy. After giving birth, the faintness with loss of consciousness continued. During a test the cause of my fainting was discovered. I had emergency surgery because gallstones were blocking the common bile duct.

The surgery was from 8:30am until 21:30pm. I came back to a room with a gastric suction tube through my nose.

I remember that I couldn't breath anymore, but I couldn't move and I felt that my strength was leaving me. I was thinking that I was dying and thought about my son. The next I know, I went outside of my body.

Then I was in a dark tunnel with people wandering around. At the end of the tunnel was a very beautiful and luminous light, but it was not dazzling.

The closer I got to this light, the more I felt relief. I felt a sensation of being loved, which I missed so much at the time. I felt very satisfied and finally happy, which was something new for me. I had positive feelings that had such an intense depth, that words don't exist to explain it. It was more like I was sensing it.

I entered the light without any fear. While I continued my way in this light, a silhouette came from above and blocked the way to the light by coming in front of me. I was surprised to see my whole, difficult life scrolling before me and I said, 'I don't want to relive this, life is too hard." And I started crying. The silhouette told me 'It's only up to you to change things, you can help many people as your soul is pure. You are needed on earth.' When he told me that I was needed on earth, I immediately was thinking about my son. Then I found myself moving backwards, but in a vortex. It was a little bit like what you can see in science fiction movies.

Then I found myself in a room. I'm looking around and seeing my body on what looks like a surgery table. But considering my medical experience, it resembled more a room for defibrillation than a surgery room. As I'm watching the situation, I see a nurse and a doctor. I hear a lot of people talking in a very noisy room next door. I feel the panic in this adjacent room from the screams and crying. I'm coming back to the room where my physical body is and I see the doctor. He starts saying, 'It's done. We lost her.' He looks at his watch and the clock on the wall in order to pronounce the time of death. At that moment the nurse asks the doctor, 'Please doctor, try once more to shock her. She's only 21 years old; she's to young to die.' The doctor looks at her and says, 'Unfortunately, I cannot resuscitate people. But okay as you insist. We will try a last time.' I see him going towards a big machine. Then I come back into my body. As from this moment, I don't remember anything anymore.

When being transported in my room, my roommate said to me, 'What happened to you?' And there I understand that something strange happened, as the nursing staff was talking about me. They were saying, 'She's coming back from far, the young one.' When trying to ask questions I got evasive answers. With time I realize that it is sounding false, as this is not logical.


