
I grew up under very challenging circumstances in an alcoholic home where I felt emotionally neglected. My brother, who was 10 years older than I, was instrumental in my upbringing. He was a kind, generous, funny and nurturing person. I was very close to him throughout my life. He was my whole sense of identity, love and family. During my teen years, I was awkward and he always told me that I was smart and beautiful and we had so many good times together. Unfortunately, in 1992, when he was just 40 years old, he was diagnosed with HIV/AIDS. I was devastated. But of course, I stood by him and supported him throughout his illness. My mother and grandmother also stood by him, but the extended family (my aunt, uncle and cousins who were like parents and siblings to us) cut him out of their lives. My mother, brother, and I were no longer invited to family functions and it hurt my brother deeply. I was angry to my core and swore that I would never speak to or forgive my relatives again for how they behaved. They were dead to me and I was going to take that anger to my grave.

On June 16, 1997, after a 5-year struggle with HIV/AIDS, my brother passed away at the age of 44. In the days following his death, I was so traumatized that I couldn't even taste food. Yet, I managed to function and handle his funeral arrangements. I felt completely numb, but I went back to work the day after his funeral thinking that it would be better to be working than sitting home alone.

I don't remember the exact date of my experience, but it was several weeks after my brother passed away. The experience happened shortly before what would have been my brother's 45th birthday. I know that it was a Saturday night and I went to bed like any other night. I began dreaming. All of a sudden, the dream began to fade and something began nudging me to turn. Everything began to feel very different from a dream.

I turned halfway around and saw my brother standing before me. He looked wonderful; he had a glow about him and his face was very relaxed, calm and peaceful. He was perfectly healthy and he was wearing an outfit that he had worn only once about a year or so before he died. It was a black turtleneck shirt, white jeans and black and white sneakers. His hair was short and he had a nicely-trimmed goatee. What amazed me is that I had liked his outfit the day that he wore it and I thought that he looked very handsome; but I never said anything to him. Of course, I never thought about that outfit again. It really struck me that he was standing before me wearing it.

My brother began speaking to me, but it was telepathic. He was speaking directly to my mind. It struck me as a much more efficient way to communicate and it seemed very natural.

'They asked me who I wanted to communicate through,' he said, 'And I told them that I wanted to communicate through my sister. I'm sorry that my illness affected you, but it was something that I had to do for myself.'

I didn't really understand what he meant by either statement, but I responded by asking if I could come with him. It was almost an assumption on my part that I would be journeying on with him, but my brother said that I had 'a lot of living left to do' and that I had a responsibility to myself to go back and continue with my life. Apparently, I had a purpose to fulfill.

'Even though we were very much linked in life,' he said, 'we have very different paths to follow. You need to follow your path. There was nothing that you could have done to change what happened to me and nothing that you could have done differently. Nothing would have changed the outcome.'

I was suddenly aware that I was in a 'room' of sorts, everything was awash in gray tones. I had the sense of a walls, but I didn't actually see any. I knew, however, that I wasn't going beyond those walls. My brother began motioning to a white marble bust on a pedestal that had no head or arms, just the torso.

'One thing that you have to understand about our cousins is that they just didn't have it in them to be any different than they were when I was sick. Look at this bust, it has no arms. You wouldn't expect a person without arms to play the piano or to catch a ball. It's the same concept with our cousins and how they behaved. They didn't have it in them to be any different.'

I nodded and looked at the statue. His words seemed to make sense to me.

'If you carry bitterness and hatred towards them in your heart, you will only end up poisoning your own life,' he continued, 'You must consider them as souls who are spiritually handicapped.'

Then in the blink of an eye, I wasn't in the 'room' with my brother anymore. I was suspended in the cosmos amidst a vast and beautiful field of stars. It was absolutely breathtaking. I could see the planets glowing and orbiting silently and I was suddenly struck by the knowledge that the universe wasn't chaotic at all, in fact it was a highly synchronous realm that had been painstakingly created by a loving and intelligent creator. That realization touched me to the core of my soul.

Suddenly, I began getting hit with a vast amount of knowledge. It was coming at me very rapidly and my mind began absorbing everything that there was to know about science, music, art history, physics and math. It was a huge burst of knowledge and I understood everything despite the rapid speed at which it was flowing. I was never very good at math, especially throughout grade school, but I found that I was able to comprehend some very complex mathematical calculations in this other realm. I knew that those calculations had some kind of universal significance. I remember thinking that everything about life and the universe made complete sense and that I finally had the answers to everything that I ever wondered about. It was an amazing feeling of infinite knowledge and it was wondrous and breathtaking beyond description.

Suddenly, I was back with my brother and he spoke to me again. We had a long talk about my life purpose but unfortunately, I don't remember any of what my brother told me about my purpose. I think that I was still trying to convince him to let me stay, but whatever he told me was acceptable to me. I had a sense of acceptance about going back to my life.

I noticed that my brother was fading and his tone became urgent.

'When you go back, you must remember that there are no mistakes in life. There are no mistakes. Money, material possessions and job titles don't matter. The only things that matter in life are love and knowledge. There is nothing else. You must remember that.'

The last thing that he told me was to take care of our mother.

My eyes flew open and I was back in my bed. It was a bright and beautiful Sunday morning and I had overslept. It was about 11 o'clock in the morning. I lived in a tiny city-row house and the streets outside were teeming with life. I sat up in bed and realized that my senses were greatly heightened. I could hear everything outside, from traffic noises to birds chirping to caterpillars chewing on leaves. It was almost a sensory overload. I also had an amazing energy coursing through me that I couldn't describe. It was just pure energy, like an electric current but not painful. It felt amazing and very, very healing. I knew that my brother was fine and that everything was as it should be.

Later that day, I went out into my backyard garden and was struck by how vivid all of the flowers were; the colors were extremely vivid. It was as if I was seeing the world through brand new eyes. When I took a walk through my neighborhood, I was struck by the people that passed by and I felt their souls. I realized then that we are all connected and that was very touching.

I wanted to write down everything that I had learned in the other realm - especially all of the knowledge that I had been given when I was suspended in the universe. Unfortunately I couldn't remember any specifics, only that I had once known everything. Sadly, the wonderful, healing energy that I had began to fade with each passing hour until it was gone. To this day, I yearn for that feeling again.
