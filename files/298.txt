

In 1976, I was a medical social worker. My primary job was to do discharge planning when people were admitted to the hospital. The lady that had the experience was one of my patients. She was in a wheel chair, in constant pain and a very frail woman. She was not happy or enjoying life due to her pain. I saw her several times before experience happened.

On the day of the experience, I was at the nurse’s station doing paperwork. One of the surgeons came in to do paperwork as well. He proceeded to tell the nurses about what just happened with his female patient. Once he mentioned the lady’s name, I began to listen because I knew the woman. He said that she died on the table and was resuscitated. When she woke up she was MAD! Screaming, 'Why did you bring me back?! I was without pain!' She told the surgeon everything that he was doing while she was gone. He said that she was watching him. She also told him about a big light. Her main focus was that she was without pain. She was very upset that she was brought back to life to be in pain again.



