

I left my body when my heart stopped beating and I flatlined.

I was rising up to the level of the ceiling and I saw my body in the intensive care room. I went into a tunnel. On each side of the tunnel, I saw deceased members of my family. They made fun of me. I heard very soft sounds and saw radiant colors that didn't dazzle me.

I was advancing towards a very bright, white light at the end of a tunnel. My little brother Philippe, who died 12 years before, took me by the hand and was smiling at me while taking me in front of the light.

A strong, but not nasty voice, asked me why I wanted to die. I answered that the absence of my little brother was too hard for me, and that my mother felt resentful towards me because of Philippe's death. Phillip died of leukemia at the age of 7 and I was not compatible as a marrow donor.

I said that I took all the medications that I could find in the medicine chest, and that my mother didn't do anything to stop me. I know that it was God talking to me, as he said that I had to go back to my family, that I would give birth to six children, and that I still had a mission to accomplish. I wanted so much to stay there because I was surrounded by unconditional love. But, I saw my lifeless body as well as the doctors doing everything to reanimate me. I brutally came back into my body through the top of my head and I opened the eyes.

I was crying a lot as I didn't want to come back.


