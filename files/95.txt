

After I lost my awareness of actual time and place, I woke up on the operating table.

I saw the medical procedures being performed on my body. I felt warmth and saw, or maybe felt, light. I realized there were other Beings or people around me. We were suspended over my body. These people were my relatives who had died. I talked to them and got advice on how to return to my body. The time when I was in that light was carefree, painless and full of peace.

Later, when I went to the hospital with flowers to thank for saving my life, I recognized the doctors and nurses who had been trying to save me.

I am not afraid of death and it is actually a positive memory. That moment was rich and full of details that escape my memory, but their effect is lasting.


