
I had surgery. Before returning to consciousness, I saw whiteness. It was like a big white sheet with the brightest light in the world behind it. There was writing on this whiteness. It said:

JESUS

RYAN (Ryan is my son who claims to be an atheist)

HELL

On my left shoulder I felt this overpowering ‘draw’ to move on, to continue ahead, to proceed, to go to the next dimension. However, on my right, I felt as if I were tethered. I felt like I was anchored and being pulled back to the present. I started pleading and begging the force that was compelling me to move forward, ‘I can't go right now, I cannot leave my boy, I cannot go without my son; please let me stay until I can bring my boy also.’ Then the pulling, compulsion, yearning, to go ahead ceased. I felt a knowledge that I was free to come ahead if I so chose, but I would also be allowed to return if that was my wish. I chose to return to a mortal life.

When I opened my eyes, I was in recovery screaming that I could not leave my boy. I said over and over again, ‘I can't go without my son. I cannot leave without my boy.’
