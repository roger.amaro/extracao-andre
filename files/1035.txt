
I was having a painful period and by mistake, I was given an overdose of a narcotic. I went to sleep and I had the most wonderful unforgettable experience of my life.

I saw my body. My mother was coming into the room and calling me by my name. She was screaming at me to wake up, while grabbing me by my shoulders. I was in so much peace at the moment, I felt the disturbing voice of my mother much higher than her normal voice. It was extremely disturbing! She left the room crying and screaming my uncle’s name. I saw him walking into the room and carry me out of the house. Last, I saw my feet dangling in the air while being in the arms of my uncle and mother.

From this moment on, I experienced a dark tunnel with flashes of neon lights of all colors, where I was traveling full speed in a form of a tornado. At the end, I saw a white bright light, as I got closer it got wider. All of a sudden, I was in a standing position surrounded by the most beautiful place I have ever seen: the peace, grass, trees and a very beautiful music with laughter coming from somewhere.

Suddenly, I heard my name as I looked around I saw myself surrounded by nurses and doctor with an IV in my arm.
