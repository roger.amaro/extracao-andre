
They had performed a Cesarean on me and had given me general anesthesia.

When I fell into a deep sleep, I saw a tunnel, at the end of the tunnel there was a door with the most beautiful and brilliant light ever viewed. The light attracted my body when I got there. Arriving at the light, I saw the most beautiful garden that anyone could ever imagine. It had many flowers of bright colors. A stream flowed through the garden. I heard the water and the sound of many birds singing the loveliest song, which can be experienced.

In the garden to the right were two people; one wore a white tunic and had bare feet. It was JESUS CHRIST and beside him was my brother who had died of a car accident when I was eight years old. When I reached them, my brother asked me, ‘What are you doing here? Jesus told me, ‘GO BACK. It is not your time. You have to go back because I gave you a mission. You’re already a MOM!’ So I returned through the same tunnel, seeing my life pass before my eyes from my childhood until my age at that moment. At the end, there was a window. When I looked, I saw my body on the table of the operating room and the doctors around me, operating.

Suddenly, I heard voices saying, ‘Ma’am, awaken. Your daughter has been born!’ When I returned to my body, I just wanted to talk about my trip to paradise, but everyone thought I had hallucinated due to the anesthesia; that it had not been anything else. It made no difference that I repeated over and over what I had seen.

The doctor who operated on me told me that nothing had happened in the operating room that might put my life at risk, but the next day, I had many doctors surrounding me and asking me questions about what had happened. Clinically I was not dead but I, yes, I was in heaven or what is best, in paradise!
