

As I was slipping out of consciousness, it was as if a curtain of light were descending.

I remember how surreal it was to realize how dark our world generally is. I'm not sure how to describe this, but I could see the world from above—and it was as if a veil had been lifted that made everything very clear and vivid. I rose up out of the darkness of the world and ascended into a very peaceful, painless, and uplifting reality. There were others there whom I could not see, but I did feel their presence. I could see where I was going to have to go next, but heard a voice in my mind telling me it was not time yet.

Suddenly, I was back in this world and noticed how really painful it was. I felt very angry they brought me back. Then one of the nurses reminded me that I will soon have a baby who will need me.


