


I was driving my mother to work and I exited the freeway in Los Angeles. I looked to my left and there were two headlights going 80 miles an hour. I was hit and killed instantly.

I felt my body being crushed and I saw my mother being thrown into the dash. I saw a light. The car began to follow the light; it traveled about 300 yards from the first impact at the intersection. I opened my eyes and did not know how I was alive. I could move my eyes and one arm so I turned off the car. I told my mother to get out of the car. She could barely hear me because I was stamped under the dash. She got out and I looked out at the night sky and died again.

I went to hell in pain and away from God. I would tell more yet I have written a story about the pain like a comet in space and the tail of the comet is your screams forever and ever. Then, I was in heaven and was in a place that words are not enough to tell of this place. I asked to see God while I was there.

In walked a person that I knew and he knew me as a close friend. I had golden-boarded clothes and a sword while I was talking to God. He was pure and perfect in every way. He was Middle Eastern and had a black hair and a beard. The flesh was of all races and I knew that this person (God) was all powerful. He looked at all humans that loved him. I was talking about earth and we both looked to my left where everything is not man-made. The darkened pit and earth was down there. Then, I looked at his beautiful garments and on one edge of the garments was diamond-trimmed with one piece that was not man-made. I was amazed and looked back at God in his eyes and then I opened my eyes.

Outside of the car, was my mother and she was screaming, ‘He is alive.’ She told me later that she heard God speaking, saying ‘lo I will always be with you.’ She ran back to the car, away from the paramedics and she saw Jesus Christ on my face.

After that, the people at the scene rushed in, grabbed me, and stood me up. I saw the world differently; I could see sin and many things that were changed. I went to the hospital and walked out two hours later without breaking a bone. Later, I saw a cross on a church and knew the man was Jesus. The man I saw was alive and there is a different symbol for Christ. There is much more and police and hospital records prove this is the case.
