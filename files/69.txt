

It had been 7 years since my relationship with my ex-partner. I left him when a new companion arrived at my work. Around this time, I was also overwhelmed from studying for a new career and working.

A week immediately before the accident, my car was having mechanical problems. But I was alone, my family was on holiday, and I didn't really think the car problems were too important.

That Monday when I went to work, the steering wheel vibrated a little. I tried to ignore it because I just wanted to get to work as soon as possible. Unfortunately, I needed to go to my new flat later that afternoon, but I realized that I had left my keys in the flat. I decided to get the keys before going to work, so I was really rushing.

The steering wheel continued to vibrate, when suddenly, I heard what sounded like a bomb going off. I was able to stop the car and pull over to the hard shoulder of the road. As I got out of the car, cars were speeding by and blowing their horns at me. I saw that my right, rear wheel had a blow out. I wasn't sure what to do next. Finally, I decided the safest thing to do would be to stay in the car. I got in the car and I started to look in the glove compartment for the mobile phone.

The next thing I remember was seeing was people with their face covered in blood. I thought they must have died in auto accidents. They were looking at me.

A bright light came to my side. I was part of this light and everything was light. I felt a deep peace that I had never experienced on earth. I had no weight. I felt alive and free.

I had a life review that moved at the speed of a ray of light. I saw images telling me about my loved ones and that I had died. I saw my family was desolate, but I didn't want to return. I said, 'I want to stay here.' Then a male voice asked me, 'Do you wanted to leave your earthly life?' I replied, 'The love of my loved ones left her.' The quiet voice then said to me, 'You have to return. Your time hasn't come. You are leaving this place and learn to value yourself.' My earthly life was in chaos, yet he knew that this motivation would make it easier for me to return.

I saw myself in a stretcher as they put me in am ambulance. When I woke up I couldn't remember anything like what had happened, who people were, where I had gone, nor where I had come from. A man in the ambulance was speaking on the phone with my mother. he told her that I was in an accident, but that I was well. Little by little, my memories came back. I tried to move my legs and thought, 'Good, I can walk.'

Afterwards, they told me the accident had happened at 15.55pm. When I looked at my watch just before the accident, it was 15.50pm. The accident must've have been 5 minutes in duration. The time I spent on the other side was as if it was a whole hour.

The man who hit me, said that he saw my car in the distance and thought it was going moving. He realized too late that I was stopped and he couldn't go anywhere to avoid a collision. He hit my car from behind in the right hand part of the car. It turns out that he was from my village, he had known my mother all his life, he had my surname name although no relation to me. However, the people at the hospital assumed that we were family.

My car came to rest on the other side of the motorway. The two vehicles were totalled. My car had the boot completely smashed up and the passenger door was smashed up inside on the passenger seat. All the windows were also smashed. The only part of the car that was intact was the drivers seat where I had been sitting. The trauma workers had said that it had been a miracle I was still alive. They ran tests suspecting ecephalic-cranial trauma, but they found nothing. I had broken rib and edema in my right foot.

About 3 and a half months after the accident I asked to be voluntarily discharged from the hospital to get back to work despite learning in the NDE that the boy at work was not going to value me. Yet, I felt such deep peace with this decision. After the NDE, the chaos of my life continued. There was so much chaos, that I hardly gave any attention to my NDE.


