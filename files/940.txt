
I, Ricardo AH, give the following testimony:

On Saturday July 26, 2014, in my parish, around 3:10 pm I was finishing lunch when I suddenly felt a sharp pain in my stomach. The pain was increasing rapidly.

What I remember:

Feeling the pain, I got up suddenly and took a step to the right side of the chair and told my companion, Angelica, who had made my food and was eating next to me, ‘It hurts here at the top of my stomach, as if I were being stabbed by a needle.’ The pain intensified more, I felt fear and something like low blood sugar; then I felt an invisible, intangible force pulling me back, but I kept my feet on the ground, as if to avoid falling back. In that moment I saw a huge white light in front of me, which grew from my central vision to wrap around me completely, I felt like I was literally taken from the reality of my life.

When the light disappeared, I was in a car with a large windshield and saw a path in which I was going towards some hills. The road was straight and long, about 20 km. The car was descending a slope into the valley. There were a few ripples on the slope as I descended. I noticed with great detail the panoramic view: the mountains, the semi-desert region, with some acacia trees randomly distributed in the landscape, but very separate from each other. I could see the color of the earth; it was like a sand color. In the background, the mountains formed a distant ridge and behind it was a huge white cloud, so big that it covered most of the horizon in front of me. The cloud was very high, with many clusters. The cloud was hit directly with sunlight, however looked completely white, with no red tones. The sky was a very deep, beautiful blue. No other clouds in the sky. That was the only one. The lines on the road were white and they were going by fast, so I knew that the car was traveling at high speed. I saw everything in great detail.

I was not driving the car, but I had the windshield in front of me, about 20 centimeters, perhaps. The car was traveling on the road but I could not feel the tire contact with the pavement. I did not see my hands, or notice how I was dressed as I didn’t see my body. I was enthralled by the scenery and modern music played in the car, I guess it was the radio or a recording that sounded, but did not distinguish what song it was or who sang. I also listened to a very sharp and discreet, barely perceptible sound: a kind of beep that was interspersed with periods of silence. It was always the same tone, but the beeps did not present identically, some were longer than others. I also heard the voice of a man who was behind me, but never turned to see him, nor distinguish what he said because my ear could not distinguish his words. I remember well that I never answered him nor said anything, but my companion was talking the whole time. After several minutes, we approached the mountains in front of us and the cloud was increasingly larger and brighter. Its whiteness was impressive and that glow was growing more and more; it approached me until it enveloped me. In the light of the cloud, I realized that its color was white, but with a slight golden hue. I felt at that moment peace, joy and absolute tenderness, as if that light burned me and gave me complete serenity. I was subjected to a love without limits comfortable as if on one’s lap. I felt welcomed, loved unconditionally, forgiven for all my mistakes in life. Everything disappeared, the sharp sound, the voice of my companion, the panoramic view, everything. Well I was there, enjoying the peace. In the light of the cloud, I didn’t see my body either. I found myself joined with the light and full of happiness. I never wondered if I was dead or not, nor thought about the people I love, or any other type of human bonding. In the light, I felt all the love I could experience for all eternity and needed nothing more. I never saw any luminous beings, not angels, nor Christ or the Virgin, or saints or deceased relatives. I saw no one there and I did not feel the need to talk to anyone. I realized that all that love, I was receiving from the light, is a love that’s given to every human being, whether you believe or not in a higher being, whether you are good or bad. I realized that the light understands all human limitations and conditions. It is above all these aspects to love every member of the human family.

Suddenly, the light around me became a sheet of ice. The ice was in front of me with a layer covering the surface of a body of water. I was emerging from the water, maybe it was a lake. As I arose, I noticed there was bright sunlight behind the ice and lit perfectly below the surface, so I thought the ice was thin. I stretched out my hands to the ice, as I approached, and noticed that there were two strands of bubbles on my left side, emerging beside me. However, I did not sense moisture in my body, nor cold water. Upon reaching the frozen surface, I was pushing it with my two hands. I didn’t hit it, just pushed again and again to break the ice and could lift it, remove it and get my head out to breathe. After about three attempts, I managed to lift part of the ice and it remained suspended, as if floating in the air above me, and I could then get my head out to breathe.

Suddenly, again, the light enveloped me. I felt transported back and appeared sitting at my dining room table: breathing hard and with effort. I was sitting in my chair, in front of my plate and I realized that there was a place setting with a place mat, plate, cutlery, and glass, next to mine and I remembered Angelica was there, but she was not. I was scared to see that I was at the table and not in a frozen lake, or on a highway, not a paradise, but in my own home. I felt a lot of fear not knowing what had happened to me. I no longer had the feeling of joy and peace I felt in the light because I was scared of having suddenly ‘appeared’ in front of my dining room table.

Then I heard the voice of Angelica, who was standing behind me, suggesting I get up and go out into the yard to breathe air. I felt peace to see that she was there with me and asked, ‘What happened?’ She was quiet. I got up quickly and went outside. She accompanied me. I did not feel weakness in my body, nor dizziness, thirst, or nausea. I felt good, but I was scared. Now when I took the first steps in the yard, my hands warmed enormously and a strong tingling ran through them. Again I felt frightened and Angelica suggested I go lay in bed for a moment. I thought it was best, but since I had the idea that I was facing an episode of low blood sugar, I asked her to prepare me a slice of bread with a little caramel. She brought it to me as I sat at the edge of the bed and waited. When she was going to give me the bread, I went to stretch open my hand and I realized I did not feel my hand, just heat and tingling and told her: ‘I cannot feel my hand.’ She looked at me silently and I took the bread from her hand, not feeling the bread. I immediately took a bite of bread, chewed slowly and swallowed. I looked at her and I realized that her lips were white. It seemed like she had bitten white sand, but then I thought they were white out of fear.

- ‘Were you scared?’ I asked.

- ‘Yes, very. I didn’t know what to do’, she replied.

- ‘Your lips are white’, I told her.

- ‘Yes, from the fright, I imagine’, she replied and remained silent, looking worriedly at me.

- ‘How long was I gone?’ I asked.

- ‘About ten seconds’, she replied.

- ‘Only ten seconds? But I got lost after I got up and told you my stomach hurt, as if being stabbed by a needle, I felt a drop in my blood sugar and then a white light came to me and I felt something pulling me back until the light enveloped me and I appeared in a car on a road.’ I gave her the same description of everything I have told.

What Angelica saw:

She mentioned that I didn’t get up before losing consciousness. She remembers that I told her about my stomach pain, as if I were being stabbed by a needle and that I pointed to the top of my stomach; all of a sudden, my hands bent towards my forearms, and these I bent toward my chest and I crouched, leaning towards my plate. She saw me and thought I was playing, but upon seeing me, still seated, and my body go rigid, she became frightened. She got up right away, moving behind me. She realized that I was struggling to breathe but I couldn’t. She started asking me what was wrong and how she could help me, but I didn’t respond, since I was unconscious. She thought I was going to have convulsions but that didn’t happen. Angelica quickly grabbed my arms and tried to straighten me in the chair. She could feel that I was stiff. While she was pulling backwards I reacted, I returned and turned to the front, sitting upright again, breathing heavily and showing fear.

After what Angelica told me:

After listening to her with attention and concern on my part, I had a second bite of bread; I chewed and swallowed. The tingling stopped and the heat in my hands was slowly disappearing until gone. Everything was back to normal.

She then suggested I sleep a few minutes, as she cleared the table and washed the dishes. Then she would go downstairs to her activities. I slept a half hour and got up to do Mass at 4 and 5 pm. I did it, feeling tired, with a sore left side that crossed my shoulder blade to the base of the chest, but the pain was there only on a small spot. After 5pm Mass, I felt much better, the pain in my stomach continued but was almost imperceptible.

This writing I made on July 30, 2014, in the first free moment that I had to write it with details that came to mind.

Here I will post what my first thoughts were about it.

First thoughts from my experience, posted on Facebook, but then withdrawn out of prudence due to the disbelief of some of my contacts.

July 30th 2014:

Some of what I felt, in that moment inside the light of the cloud: I had to leave what’s temporary to experience the eternity of absolute love. In this moment, that I live now, the temporal and the eternal converge. Love transcends time; it stops it and becomes everything in an instant and forever. Just as one cannot hold on to the flow of time nor can one hold on to the uncreated love that comes to you at every moment. That which is Life lives within you and waits there, but when you go within yourself and look for it, you can feel it but not hold onto it. That cloud was more grandiose than anything was; its light was bathed in peace, a hug of unconditional love that received me in its embrace, despite my errors in my personal life. The love that I felt is the greatest that I have ever received. I felt my insignificance, but I also felt, gratitude for feeling welcomed by the light. I don’t know if anything stuck with me, that I am so limited and unable to delve into the mysteries, but I trust that light, that cloud: symbol of the presence of the Eternal. I hope that my life, or what remains of it, is done with gratitude, peace, surrendered devotion to that which is Beauty: both old and new, which took my time to love. That my decisions arise from this meeting, which was not one instant, but keeps giving every moment that I breathe, every beat of my wounded heart.

August 3rd 2014:

The NDE I had on Saturday July 26th has helped me to value each moment of life, the beauty of each moment; I have a feeling of admiration for each life; it has helped me to accept who I am, within my limited reality. It has made me more compassionate, understanding and helpful to others. All material things seem empty to me, without meaning; I no longer have that desire to compete for anything, nor get along with everyone; I have more desire to be at peace, meditate, pray; the fear of death has gone because I know what awaits me, though I retain a slight fear of what could lead me to death. I have more awareness of myself and living in the moment; I feel I am healing inside, although that means a duel between old patterns and behaviors and breaking paradigms that I had for years. I also live a different sense of the flow of events, of dealing with people. Everything seems new, as if I have come from another country to settle down for the first time here and I need to adjust back to everything I did before, but now I have another perspective on life. It is strange. I feel my life has new meaning, a new purpose, a need to finish something entrusted to me. There are many things I'm still learning from that experience. It’s not assimilated all at once, it takes time.

What I learned from the experience.

When I left:

- Death arrives at any moment, without warning.

- It is impossible to escape the feeling of the light and falling into the void: human beings are helpless before the mystery of death.

On the road:

- Feeling of total detachment, absolute detachment from all past, respect for all people in one’s life; a sense of tranquility.

- On the way I'm driven, but I feel safe despite the speed.

- Off the road toward the cloud, everything is semi-desert, almost lifeless.

- The picture gives a sense of tranquility.

In the cloud:

- The cloud can represent a Divine presence.

- The brightness of the cloud comes towards me and engulfs me before reaching the mountain.

- Sensation of being received by the light: unconditional love, tenderness, peace, joy, security, forgiveness, living in quietude. Nothing is necessary any longer; I yearn for nothing. Pure happiness, without longings, without memories of the life I had, without worries for those left behind.

In the frozen lake:

- I am responsible for my own life: I need to take charge of it without trying to resolve anyone else’s life.

- Only I can fight to move ahead and be reborn. I feel the light has given me the strength and knowledge to do so.

- I sense that I must overcome my previous beliefs and patterns and restart my life, strengthened by the experience of light.

Upon returning:

- Feeling of being thrown back into the world, without having asked to be.

- Feeling absolutely new to all that I perceive every day after the NDE.

- I have a new perception of the flow of time.

- I have the feeling of having a ‘reset’ in the form of looking at life, the way to appreciate every moment.

- I have the emotional need to mourn what was lost in my life. It is a duel without nostalgia.

- I feel like my past is dead, I honor it for all it has taught me, but I let it go grateful to God for all that I’ve lived. I feel like I have died and been reborn.

- I feel the need to adapt to a world that does not know my experience or can even imagine it: A world that I see with new eyes, but cannot even describe. I only know it's the same world as before, but I no longer see it with the old perspective.

- Everything is new. I have trouble relating to people, with the world, with my activities like before.

- I tend to re-do old habits, but they no longer have meaning. I have to stop myself and do things slowly (walking, breathing, speaking in a way that I don’t feel ruined by my own habits) and create a new existence based on what I lived in my NDE.

- I feel alive with the certainty of having already been to the Mystery of God, being in a kind of divine house. But I don’t know how to express it. I feel the need to live in humility. As I do not know how to formulate it into words, I quiet myself, meditate, breathe, think, walk slowly, live deeply in the present, but do not know how to express the mystery. This is an ineffable mystery. I feel a certain sadness of not being able to communicate it, but I perceive it as a gift from above, it is a personal and undeserved gift because I am no saint. I get the impression that others will not understand me or not give importance to my testimony and I think: ‘I can tell it, but I can never communicate my experience. I know this, I admit. This limitation is part of the journey of faith.’

- I see others with compassion and wonder if they have any idea of the essence of existence that the light gave me to experience. I get the impression that they do not realize this and therefore live distracted, thrown to the interests of the world. People, who do bad, I feel, are lost because they have not felt the Love.

- The silence, meditation and prayer allow me to live what I experienced in the light, but it does not reproduce the actual experience. I can only conclude that it was made by the light, amid a situation perhaps physiologically critical. Perhaps that is how the human brain was designed.

- Coming back feels like a rebirth, but now everything is different. Much of what once mattered to me now does not make sense, both, topics of interest and relationships with certain people, who were almost indispensable to me and with whom I had an attachment bond. Now I feel it no longer makes sense to miss such relationships or such topics. What I experienced in the light of the cloud in the NDE is an absolute value to which everything else is worthless.

- I guess the way to embody the mystery is silence, fasting of unnecessary desires, the prayer of thanksgiving and being available to the light that engulfed me, loved me with a personal love. I intend to meditate, breathe carefully, walk mindfully, treat everyone and everything with extreme care, love people with heart, but not get involved in the solution of their lives.

Note: For me that light is God himself. I don’t write in the text that it was ‘God’ because what I saw was simply light.
