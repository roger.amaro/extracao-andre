

I was swimming in a flooded gravel pit with a girl who was a friend. It was a neighborhood swimming hole. She started swimming out to the center and I began to follow. I was a smoker and did not realize how winded I was getting until I was too far out to return. Upon realizing this, I turned to swim back. The lactic acid build-up in my arms became unbearable and I began to panic.

It was as if my mind separated from my body. I became an observer, as I had no control over what was happening. Very quickly, I tired and gulped a lot of water and then breathed in the water. I no longer had a desire to breathe and all resistance ceased. As I began to sink to the bottom of the pit, I felt intense sadness that my life was ending and that my parents would be hurt immensely. At 3 feet deep, it is pitch black and I could not see the hand in front of you.

However, as I began to die, things got brighter when I dropped to about 15 feet deep. I was on the bottom of the pit and able to see everything around me. I started to get curious, as my mind did not feel like it was dying. I began to accept death and was not scared. At that moment, a voice, spoke to me and said this. 'Jay. It is not your time.' It seemed loud; yet, how could it be? Something took control of my arms as I lay spread eagle. I was pushed up like a bird flapping its wings. As I approached the surface, I could feel control start to come back.

I broke the surface near the girl who had been looking for me. She said it was pitch black. Anyway, I grabbed and started to pull uncontrollably on her. Then I had to push her away, so I would not drown her too. Again, I went under. When I came up again, she was behind me and started kicking me in to shore. Mind you, I was not breathing yet since my lungs were completely full of water. As she kicked me in the back, it helped to get the water out of my lungs by forcing me to cough when she kicked. Eventually, she kicked my to shore as I was too weak to swim to it. Then, I vomited a huge amount of water and then fell asleep for 3 hours as she watched over me. She told me I had been under for 5 minutes and she was going to get help when I surfaced.

That experience led me to believe that while we live in this world, we are also watched over by something. I remember feeling acceptance at the realization that dying was only the end of the body and not of me.

