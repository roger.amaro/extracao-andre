

I had a back surgery in May of 2012.

Just shortly after surgery began, my lungs collapsed and I went into full cardiac arrest. My doctor started what he called 'The death clock', which apparently is engaged when someone dies on the table to know how long the person is dead.

After a crash cart jump start, I was gone for 1 minute and 57 seconds.

After they completed the surgery, it took me almost 7 full hours to wake up. When I did wake up, there was a nurse right above me and I asked her this question, 'Am I dead?' She assured me that I was alive and then left to get the doctor. While she was gone, I KNEW something was different or wrong with the world. It was confirmed when a doctor told me the entire story.

After that, I had a few people come into my room who I didn't know. They asked me what I saw or if I heard anything. I told them exactly what happened.

I remember it was very dark, very quiet, and very cool. It was SO VERY PEACEFUL.

I didn't see a light. I didn't see my grandparents. Nor did I see all my long-gone pets. It was just dark, cool and quiet. I really hate bright sunlight, loud noises and heat, so dark, quiet and cool is heaven for me.

After I got out of the hospital, I was suddenly not worried about dying anymore because I've been there. It's just exactly what I would like to spend eternity doing, being in a dark, cool and quiet place.

Since then, I have shared my NDE with a good number of people who ask me about it. I don't advertise it.

The religious people I've told my story to are interested, but feel I 'Wasn't gone long enough' to have the white light experience, or they would say it just wasn't my time. The non-religious people I spoke to believe that is EXACTLY what will happen to all of us. That we will all go to a dark, cool, and quiet place. I did have one religious person tell me that I was in hell. However, if I was in hell, there'd have been bright, burning sun, loud noises everywhere and it'd be about 1,200 degrees farenheit.

I was very confused when I woke up, but I'm not now. I am very peaceful and quiet. I'll be very ready for this moment, and I've told my loved ones that I'm going to my heaven.

