
I was painting a room and tore a cartilage of an ear. In a few days, my ear became infected. I went to the doctor who prescribed a strong antibiotic injection. They gave me the medication in a certain house two blocks away from my house. On the third daily injection, I passed immediately from the place where they had just given me the medication, to another place that I didn't recognise.

This was a speedway autodrome for racing cars, where there had been an accident. I was here seeing everything. I realised something special had happened. I looked around and saw a mountain of tall trees in the distance. I said to myself that I had to go to this place. Without knowing why, I tried to go over there but couldn't because of a transparent film in front of me. I couldn't cross the film or break it. When I tried to go forward, this film stopped me. It enveloped me like a glove. I returned saying slowly and firmly to myself, 'I MUST RETURN!'

Next, images of my mother and daughter appeared, as if I was thinking about them. Again, I said, 'I HAVE TO RETURN.' This time, I was able to break the transparent film. I appeared in a very bright tunnel. It looked like luminous acrylic paints of different colors were falling. I went through the tunnel. I felt very happy and was aware that I was returning. When I regained consciousness and came to on the floor of the nurse's house I was saying, 'I must return.' The nurse told me I had been on the floor for about five minutes without a pulse.

After a number of years, I ended up working as a cameraman of motor racing in Argentina.
