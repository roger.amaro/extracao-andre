

My experience two days ago was the most terrifiying feeling I had ever encountered. I lay on the cold ground as the world went on around me. I could hear every sound, every footstep and every breath of my friends closest to me. I felt my heart beating like nothing before. I felt myself losing touch with reality. I couldn't move. All I wanted to do was to scream and yell, but I couldn't. I started to feel myself fade farther away but yet I knew exactly where I was lying. Although, I was there, I was a million miles away. My friends tell me that I blacked out, and was unresponsive. Yet, I can tell them exactly what they were talking about, where they were, and what they had in their hands at the time.

I saw everything, despite my eyes being closed. I tried to hold onto memories, but nothing was working as everything was fading. Then, something was with me. It terrified me. I saw darkness that felt so dark and evil. I was trying to scream but I couldn't. Everything I tried to hold onto faded while something was pushing me and telling me to move go with them. I felt such a horrible presence that it terrifies me even now as I think about it.

I woke up in an ambulence and was told my heart had stopped for 12 minutes. I was told that I had no pulse or brain moment. I feel like I have been shoved into a body that is no longer my own. I feel like nothing matters and I am having problems readjusting.

