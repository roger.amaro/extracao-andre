

My family and I were on vacation at Chesapeake Bay. We were in the water and I had learned how to boogey-board from another girl around my age. My family had gone out and purchased one so I could use it the next day.

I was using it when suddenly a wave larger than I had expected swept me under the water. I remember floating under the water. I could feel it all around me. I was the most relaxed I have ever been in my entire life, before and since this day. Everything was super-calm. I remember wanting to just remain there forever.

My Dad was lifting me out of the water and all the feelings of life came flooding back to me. I remember shouting at him. I wanted to know why he pulled me out because I was so happy there. He told me I had been under the water for a long time. I had always been able to be underwater for a long time, so for him to be concerned about how long I was down there meant something.


