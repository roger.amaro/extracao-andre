
It was during my lunch break in first grade. I was in the monitor office playing with the toys. She had a pot full of candy on her desk. I took one, a hard one that takes time to melt, and went back to playing with the toys.

I swallowed the candy not long after, but it went in the wrong hole and it was stuck. It was lodged kind of at the entrance of the breathing hole, and I couldn't breathe anymore.

I stood up and put my hands on my throat by reflex making a choking sound. That's when it happened. I was seeing myself from the top of the room, from the front, three to four meters away at forty degree angle in the air. I had a full view of the room.

I saw the lady behind me stand up suddenly from her desk and rush at me. She tried to get the candy unstuck. She succeeded. When the candy was free, I went back in my body. The whole incident lasted ten seconds at most.
