
I dove into the deep-end of public swimming bath on holiday, but I couldn't swim. I began to drown. As I thrashed and panicked, a great calm came over me with a sense of 'all will be well'. This occurred during the underwater phase of drowning.

I sensed peace, calm; love awaited me upon my death. The intervention of an adult, who pulled me out of the water, simultaneously snatched me back from a growing sense of calm and peace. While 'calmed' I was outside my body, looking at me thrashing in the water. Slowly, I felt myself drift even further away, still watching me.

Only when the adult jumped in to save me was I instantaneously 'back' inside my body; rapidly so, as measured in nanoseconds. The beauty to this tale is its innocence. I was a child, totally 'virginal' in terms of NDE experience or prior influence. The effect, post-trauma, has stayed with me for my life to-date. It’s REMARKABLE.
