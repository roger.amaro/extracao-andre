

After five hours of tachycardia and atrial fibrillation, my emergency room doctor was concerned that I may have a stroke. A decision to perform a cardioversion was the suggested remedy. As soon as I was put under, the cardioversion was administered.

I immediately was standing at the entrance to a long, dark tunnel. I witnessed an incredibly bright light at the end of the tunnel. On either side of the bright light, vivid colors of red and yellow were displayed. I saw red on the right side and yellow on the left. Red and yellow are my favorite colors.

Then I woke up and cried out to the medical team around my bed, 'You should have seen the colors!' I know that light was and is Jesus. I know my two sisters were waiting for me behind Him, but it wasn't my time. I felt such peace and thank God for this beautiful experience.


