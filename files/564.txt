
I broke my nose when I was 7 years old. My Mom picked me up from school and took me directly to the doctor. After he checked my nose, he told me that I could only have rhinoplastic aesthetic surgery after I reached 18 years old.

When I turned age 18, my mom and I went to the hospital for surgery. Very early in the morning, the doctor's nurse took me to the operation room. They helped me to lay down on to the surgery bed and the anesthesia doctor injected me with the anesthesia. I was under general anesthesia.

After that, I saw a very deep Black tunnel and there was an incredible light at the end of the tunnel. I could see some shadows that were like people, but I could only see their shadows. It sounded to me like they were waiting there for me at the end of the tunnel. My head was turned towards that incredible light and my earthly body was rolling towards right side and left side. Some of the shadows were long in height, while some of them were short and some were medium height. I saw about 15 to 20 shadows of people. I couldn't reach to those people and suddenly felt my soul like it's vacuumed back to my body. As I was trying to open my eyes while I felt the doctor's hand pressing on my nose to stick bandages around my face.

I remember that my whole body was so cold. Mom was waiting in front of the operation room. She was whispering prayers. Then, everyone altogether took me to my private room. My mom was thanking God continuously. After I came to, I told everything to Mom.

She said to me that it's was three hours since I was in the operation room. At the end of the operation, the nurses were in a rush. They were hurried and nervous, carrying many oxygen tubes. She said to me that she asked the nurses if those oxygen tubes were for me. They did not reply back to her. After months passed, I talked with one of the nurses who were there for my operation. I have been told that I woke up in the middle of the operation and anesthesia doctor injected me with a second shot with a much higher dose. Then they lost my Heartbeat, my pulse, and then my blood pressure.

All I can say is that it was a great experience. I didn't feel any pain. My whole body and my whole soul was in peace and joy. It was a Great Feeling! I am not afraid of dying because I assume that people meet their loved ones and relatives there. Thank You.
