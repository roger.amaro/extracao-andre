
I hit my head on a bar and fell backwards. I felt my heart stop beating and everything went black. I put my head on the ground. It was night and everything was dark.

I blinked my eyes and saw light. My heart began to beat. I got on my knees and my head went all the way back. I was in a lot of pain. I asked God to let me into heaven. A tunnel came into view and it was smoky. Further up into the tunnel, I saw a light and it began to clear. I told myself I was glad that I was going up instead of down. I saw my body lying on the ground but my eyes were level with the grass. It was daylight where I was.

I saw an elderly white haired man sitting in a chair with a boy with long hair sitting next to him on a stool. There were woods in the background. I remember most that the elderly man told me to make peace on earth because I couldn't stay yet. I told them I would see them again and then I re-entered my body.

I was taken to the hospital and got four stitches in my forehead. The scoutmaster took me home. My senses were very keen. I began to see into the future. I told him I would be going into the US Marine Corp when I was 18. I told him to give me a squad of men in Vietnam and give an NVA Colonel a squad of men and we would have a show down and find out who has the best way of coming up because I would be coming up with God and Jesus Christ. This event came true as I saw it 6 years later. I joined the Marines and went to Vietnam. The NVA colonel and 14 men with him came down a trail where we ambushed them. We killed the colonel like I had seen 6 years earlier.

I lost my memory of the NDE until a few years ago and it all started coming back. It happened just like I saw in detail that night so long ago and just what I told my scout master in 1961.
