
I was a 19 years old, charming and energetic girl preparing for my University exams. I know the date and time as well. I watched the program called the 'The World This Week' with Prannoy Roy. There was a session about NDE. My parents, elder sister and youngest brother were sleeping. My other brother, Anand, was exercising lifting, weights in my room. I resumed my studies, switching off lights and the television.

We had a casual chat. He questioned me, 'Hey, what will you do, if you are about to die now'? I said, 'I just now watched something on death; I am not scared. You can try.'

He was 18 years old, strong and well built. He told me, 'I do not believe. I shall hold your throat, you won't be able to breathe, and then you will kick me.'

I said 'Go ahead, why not?'

He held my throat and I could not breathe. But to win the challenge, I said to myself 'let me see, how I am going to face this?'

I felt light, cool, pleasant and indescribable. I saw soft soothing bright blue light and felt somebody smiling. I saw a figure with human male features, golden globes all around and felt like I was warm comfortable in this form. I saw myself seated on a wooden armed chair in front of a wooden table.

I was wearing a light pink knee-length skirt and an orange shirt. My brother was stroking my cheeks. My father, mother, brother and sister kept sleeping. Although I could see, it wasn't quite like seeing, but rather a knowing. My brother was crying and feeling so miserable. I could hear him saying 'NO, I killed you Kashi.' (Kashi is my nickname.) I wanted to melt into that beautiful light. But, I said 'I can't do this to Anand, I want to go.' The man's face was still smiling. One thing about that face is that it was not as if it was made of skin and flesh of any sort. It was like shapes you notice in clouds, but it was full of beauty, serenity, and completeness.
>br> I just slipped into my physical body. I could feel the shake and relief in my brother.

I saw him and smiled. He asked 'Are you alright?' several times. I kept telling him, 'I am okay.'

He said he was sorry. Then I went to bed and slept.

But that experience remained with me.

My brother later shared that, my eyes popped out and when I regained my posture, I closed my eyes. I talked in a strange language, which he could not comprehend and had not heard before.
