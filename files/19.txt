




My childhood friend was on her deathbed many miles from where I prayed. She was dying from Cystic Fibrosis. Her sister just informed me that they had stopped the feeding tube and her heart was stopping. I began to pray to God, Mother Mary and Jesus that her soul would find heaven. I was so worried for her, and her soul.

As I was praying, a vision appeared. I saw one of my friend's green eyes and a tube-like cord protruding from her pupil. This cord was dark black, with beautiful lights under the outer layer and moving all about this cord.

After seeing this from an outer perspective, I was now moved into the inside of this cord-like structure. Now I was in the cord/tunnel, which was typical to what most near death experiences describe. The tunnel was grey in color, and had ridges and ripples. I was drawn to a beautiful white, brilliant light and towards the door. The movement was golden colored. I was very aware that my children were asleep next to me in bed. Yet, I was full of curiosity to reach the light. I thought I would die if I entered this threshold of amazingness.

Then everything went dark, except for a few spiraling stars. I realized my friend had died at 11:32 pm, which was the same time that this vision Occurred. I have never experienced anything at all like this. However, now that this has happened, I am opened up to spirit.

