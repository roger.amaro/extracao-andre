
I was on a road trip to holiday with my then-girlfriend's family on 25.12.2011. We were taking a scenic route, through national parks and the like. I wasn't stoned at the time but had residual effects of psychedelics (LSD, mushrooms and DMT in the previous months), and of marijuana from the day before affecting my system. I was out of sorts because a close friend had died and I'd exacerbated the grief by taking too many substances. I was also in unhelpful circumstances. (I now take no drugs except the odd drink and daily coffee).

We drove past a turn-off to a road called 'Paradise Point' or something similar, so I said 'How can we drive past Paradise Point?' We pulled over to do a U-turn. My girlfriend was learning to drive, so I used it as an opportunity to teach:

'First we indicate, then technically we wait stationary for five seconds. Ok now we check the blind spot and the traffic in front and...'

I saw a 4WD truck moving fast not three meters away and accepted death, then lost track of identity/time/physical coordinates/thought.

(I have thought for five years now about how to describe the experience, and discovered that English language presupposes time. Try to construct a complex sentence without a linear implication. See? But I'll do my best with flawed language.)

Immediately there was relief. I say 'there was' and not 'I felt' because the 'I' was a distant unremembered trial, something gone, something behind, let go.

Relief. Twenty-seven years of moment-by-moment input, frequent confusion and struggle, and mistakes and shortcomings punctuated by good things… and then relief. As my identity vanished, I became aware of my surroundings. 'Surroundings' presupposes existence and physicality, but this may have been infinity. Instinctively it felt like the sum total of infinity in its pure, a-temporal, undifferentiated state(s).

There seemed to be a swirling color that was luminous but in an absence of light. It was somehow indistinguishable, as if un-illuminated, perhaps in a vacuum. The paradoxical aspects arise while describing the experience, but the experience itself was pure and without resistance or tensions.

I welcomed this experience and agreed with myself to stay, contented.

But soon I was curious to search for parameters, a threshold, and new knowledge. I was getting daring. But having a will seemed at odds with this reality, and so I observed gratefully and wondered if eternity here could please be my new way.

I felt enveloped in love, and indistinct from it. Unbelievably beautiful was the experience.

If I had to name the sensation or color around me, I might say blue or purple, though I know this to be misleading, as it wasn't a quality or reflection of light as perceived through a mind, it seemed to be a total experience.

I came-to when I heard a bloodcurdling scream from my lover. That sound was enough for me to reconnect to my identity, though I immediately felt loss for having to leave egoless bliss and love sensations and return to compromised and distorted perception and a flawed identity.

I have since wondered if my experience was the perceptual manifestation of my deepest desire for there to be an experience of being immersed in a benevolent infinity. Of course, that suggests a body, but I mean as a rejoining with pure potential, beyond time in a larger Meta state that (usually?) eludes human perception. Perhaps, if I had a deep desire for another kind of experience, that's what I would have gotten. Because those were either the deeper layers of my mind that surfaced, or maybe there’s a benevolent force.

