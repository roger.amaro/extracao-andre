
I was 8 or 9 years old. I was in class but the teacher had left the classroom. My friends and I were playing. As I ran through the passage, one of my friends hit me. I fell on my back and my head hit the ground hard on a part of the neck.

For a moment everything went black. Then there was a white light. I saw the faces of my friends who were asking if I was dead. I could hear them and I saw a couple of them try to touch my face to see if I was OK. My hands moved away from them. I then saw myself on the floor. I saw everything had gone white again. Then I awoke on the floor where I had fallen.

I didn't know what was happening. I heard and saw people trying to resuscitate me. I saw myself but I didn't float; I didn't see a tunnel or a heavenly voice; nor did I see anything like that. I just listened to the people there, but they couldn't hear me.

Today I am 38 and never again did I have an experience like this. I don't know anyone that this has happened to. That is all that I remember.
