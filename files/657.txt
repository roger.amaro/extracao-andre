
When I awoke in my hospital recovery, it was like night and day. I felt like I was given new life and I felt full of life with blood flowing strong, breathing normal, and no pain. I got out of bed and started to dance. That movement caused the groin artery incision to blow out like a grapefruit and it almost killed me. A male nurse had to apply direct pressure for eight hours and saved my life. I was out of the hospital within a few days when I remembered my very disturbing NDE came to me. It was not in a dream, but an awake memory.

I was watching as the two paramedics wheeled me into emergency surgery. Then everything went blank.

The next thing I knew was that I was suspended in a vast black darkness. I was terrified, heading towards a black mist. It was not a wet mist, but more like a black fog. At this point, I am watching these events from both inside my body as well as outside my body. As I enter this fog, feet first on my back, the feeling begins to leave my body starting at my feet. The feeling creeps towards my head. I am terrified as I am surrounded by vast darkness and black fog. I am screaming and crying, 'No! No! I do not want to go! Please God, I don't want to go there!' I have never been so terrified. As the fog passes my legs and moves upward to my midsection, I am losing feeling in my arms. The panic really sets in. I continue begging God to please help and pull me back out of this blackness. A presence grabs my upper body and pulls me out of the fog.

This daydream only happened once, but I remember the actual experience like it was yesterday.
