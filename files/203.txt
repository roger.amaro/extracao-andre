

I was 6 years old and living in California. I became ill with a virus or something. I was in my bedroom and in my bed. I had been sick for several days and had a headache with terrible aching; all the symptoms associated with a high fever. I felt exhausted. I remember my mother being there trying to get my fever down with wet cloths and trying to get me to take sips of water. I was sweating a lot and began shaking all over. She called the doctor.

This was back in the day when doctors made house calls. He happened to be just finishing with another patient relatively near to us. I remember hearing the relief in my mother's voice as she told me the doctor was on his way.

The doctor arrived, examined me, and tore off my clothes. He ordered my mother to get trays of ice cubes. I rose up out of my body and up to the ceiling. I saw him carry me into the hallway and I followed while still on the ceiling. The doctor carried me to the bathroom and began to fill the bathtub with cold water. He set my body into the water even as it was filling up. I was seizing. My mother came in and they put the ice all around me.

I don't remember what happened directly after that, but eventually I was carried back to my bed. My body was no longer seizing, it was just limp. The doctor was doing something to me and my mother was sitting at the foot of the bed looking really worried.

I think at that point, I don't remember because I began to rise above the ceiling and then I went into another realm. There was a lot of light. A young woman with long hair dressed in a beautiful, flowing whitish-blue gown was there close to me. She emanated complete, loving kindness and I thought she must be an angel. She communicated with me through our thoughts, not by speaking. There was light behind her and I was so drawn to that light that I began floating toward it. She stopped me, put her arms around me which filled me with light and love, and said, 'It is not your time yet. You're going to have to go back.' I knew she meant go back to earth, to my bedroom and my body.

I really, really, really, did not want to go back. With her, I had no pain. I was filled with love, joy, and light. I told her, 'No. I want to go with you.' She smiled and gently said, 'I'm sorry, you must go back. You still have things to do.'

I didn't know what she was talking about. I remember thinking, 'What things?' as I resisted going back.

I repeated, 'I want to be with you.' She still had her arms around me and said quietly, 'You will see my face among the Grieved. Then I had a vision. I could feel that I was much older and I was in a place where people were milling about in shock. There was rubble and chaos everywhere. I had no idea what it meant and I didn't particularly like it.

Then I was drawn back down into the room and back into my body.

Coming back into my body was horrible. I felt pain, while my body felt very small and restrictive. I felt really sick. I did not want to be there, but didn't have a choice. I was unable to get out of my body. I was drenched with sweat and the doctor was still working on me. I remember him turning his head to my mother and telling her that I was going to be OK and she began crying.

I remember thinking, 'This is not OK. I feel horrible and I want to go back to the woman of light.'


