

It was Wednesday morning and I was on my way to work. I hadn't eaten that morning because I was running late. At the time, I was 21 weeks pregnant. A couple miles from home, the gestational diabetes kicked in and a drop in my blood sugar level caused me to pass out behind the wheel of the car. I was going at least 55 mph, when I crashed head first, into a flatbed delivery truck. I remember at one point, I came to slightly in the car. I noticed how my body was positioned in the car. I thought to myself, 'It feels like I'm lying down in the back seat, with my legs draped over the front seat. I must be taking a nap on lunch. I really hope I set an alarm. I don't even remember making it to work,' and then everything went dark.

That was the first time I flat-lined. I have another memory of being loaded into the ambulance. I do remember repeatedly screaming, 'I'm pregnant, I'm pregnant!' I was hysterical. It went dark and I flat-lined again.

I woke up in the hospital, and they told me I had been in a car accident. They asked if I knew my name. I told them, 'Elaine.' When they asked for a number to contact my family, I could only remember the number for the baby's father, but he was in another state. They called him, and he eventually got in touch with my family.

I was in and out of consciousness as I underwent 16 hours in several surgeries. I had severe internal injuries, including a ruptured uterus that necessitated a hysterectomy. My baby girl died from the impact. My kneecap was shattered, the hip and femur bones were broken. My ankle and pelvis were also broken. I was cut, swollen, and my lungs were filling with blood. It's a complete miracle that I'm alive now.

The third time I died is the one I remember best, though all of the experiences were the same. I woke up, and found myself on a ventilator. My lungs had collapsed during a surgery. When they removed the breathing tube, my lungs refused to work on their own again. I kept trying to take in air, but I couldn't. The world started going black. I remember thinking to myself, 'This is what it feels like to die.'

When I died, I found myself surrounded by nothing but black. I couldn't see or hear anything. I had no knowledge of my death, or even any thoughts about existence at all. It was like being asleep, only there was an absolute consciousness such that I was aware of myself. I was aware of everything around me in all of the darkness. But I felt like a part of it; like my entire being was only a single atom and I was connected to all the atoms of around me. I felt nothing but serenity. I was part of all that surrounded me, and I was content with that.

When they shocked me back to life, I woke up to teary family members and relieved doctors. Over the next days, I remained fatigued and underwent more surgery. I was in the hospital for three weeks, and I have not forgotten that experience. It is a couple weeks from the first anniversary not only the day I died, but the day my daughter died as well.


