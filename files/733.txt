
Usually I would be afraid to go into an operation and my daughter (Michelle L. whose NDE account is #3965) tells me she'll pray for me. Yet, I was unafraid as I went into the operating room.

They said the heartbeat going and then they saw a straight line on the monitor. This is probably when the NDE happened to me.

I had experienced going straight to heaven. When I went there, it was a wonderful experience. I had no pain at all. I saw a light that I can't explain but it was the brightest light I've ever seen. There were two brown-haired children sitting at a table; one was a daughter that I had miscarried and the other was my daughter Michelle L's miscarried son. They were grown up. I was happy to see them and they hugged me. My grandson, he was a little bit bigger than his two brothers on earth. My daughter Susan, looked just like Michelle her sister. I didn't know the reason why the miscarriages happened, so always had a sense of worry as to what became of them. They looked like they were very, very happy, which made me feel a lot better. Both children looked at me. My grandson said, 'Grandma, go back, we don't need you.' My daughter said, 'Ma, we're happy, we don't need you. Go back.'

When I came back, I was in a lot of pain. In a way, I was disappointed to come back because of the pain I was in. After a few days, I was starting to feel better. And that's my experience.
