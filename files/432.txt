
I was on my way home while riding my bicycle. I had just ridden over a bridge that passed over a reservoir. Suddenly, I felt so unclean inside. The next moment, I saw a rock about the size of a fist on the road as my front wheel hit the rock. I went flying over the handlebars and that's the last I remember. I must have landed on my head. I was in total blackness, and barely aware of anything.

Suddenly, it felt like I was being lifted straight up and super-fast by my elbows. The sound was like a loud, deafening wind that sounded like a train engine. I found myself standing on the bridge in front of an elderly couple. They were very well-dressed in black and I felt like they were on their way to church. In back of them was a very old black car, like the one in this photo:


I was dumbstruck and in awe because there was such a feeling of kindness and caring from them. The woman said, 'We weren't sure you were going to make it.' I was enveloped in the feelings of kindness coming from them. Then the woman said, 'There's your neighbor.' I turned and saw a neighbor, who had fallen to a seated position on the little hill on the side of the road. Then the woman said, 'Now, you go home...!'

So I picked up my bicycle and went over to my neighbor. All of a sudden, my head was filled with incredible pain. I felt so much pain that I had to sit down next to my neighbor. I buried my head in my hands for a long time.

Many times as an adult, I have wanted to locate that neighbor and ask her what she witnessed that day. But, I never have.

From that moment on, I had amnesia. My neighbor walked me to her driveway and then sent me on my way. As I walked home, my father's best friend talked to me. I didn't know who he was or what he was talking about. When I got home, I went upstairs to tell my mother that I had hurt myself. But she was busy on the phone.

I went into the bathroom and looked in the mirror. I was horrified and terrified because I did not recognize myself! My mother must have sensed that something was wrong, so she came in. I told her I had hit my head. She took me right to the pediatrician. He found a lump on a bald spot about the size of a quarter on the corner of my head. He commented that hair will never grow on that spot for the rest of my life. The area is smaller, but still bald today and is still very sensitive to the touch.
