

During my third back surgery, I became completely aware and the memory is as clear as I type this. I was fully cognizant. I figured something went wrong with the surgery because I could see THE light. I saw my grandmother, Agnes, and my wife’s grandmother, Millicent. They looked like the best version of themselves. I realized that I could go if I wanted to do so. Agnes, pointed for me to go back.

I really hated that mean woman, but now she’s my guardian angel. She had an incredible energy and used it negatively. She understands better now and I channel the energy she gives me in a positive way.

So I headed back to my body. I had to concentrate visually. The best I can describe it is it’s like moving through the lightless void of space. I knew to keep looking for my hand in front of me and could eventually hear the nurses calling my name.

If you want more visual detail, I’ll send a video or something because I hate typing and avoid office work at all costs.

