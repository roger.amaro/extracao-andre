

I flew head over heels while training for a cross-country bicycle race. My chin hit the ground. Luckily the ground was sandy and soft, otherwise according to the neurologist I would likely have died. During my fall I flew up high and it became dark.

Then I stopped rising and found myself in the cosmos. I could see the earth as a huge ball. It was spectacular and hard to explain in words. Then I realized that this is impossible! I need to be down there since I had fallen down. With this thought, I flew at an unknown speed toward the earth in a fraction of a second. But I did not go back into my body. I lingered in the air between the trees above my body. Then I thought, I need to go lower. Immediately, I shot toward my body. I re-entered my body head first and then slower, I entered the rest of my body. It took several minutes for my left leg to go back into my body.

I wanted to stand up but quickly discovered I couldn't move. My hands, arms, feet and legs were totally numb.


