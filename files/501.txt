
I worked as a technician in an ammunitions factory, making ordnance for the Vietnam War effort. I dropped a large portion of mines on the production floor.

A male voice said 'You are born' or 'It begins.' Then a deck of cards appeared. The first few cards were black. Then every card after that was a snapshot of everything important that I have ever done up to the moment of the accident.
