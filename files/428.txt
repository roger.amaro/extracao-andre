
While in the Intensive Care Unit of the hospital the doctors inserted a feeding tube into my nose while I was intubated. The nutrients from the feeding tube filled my belly causing my stomach to spasm and all the nutrient went into my lungs because I was intubated. I felt my lungs fill and my breath shorten. The call-button was just out of reach when I realized I had no control and there was nothing I could do. I thought of my family and the life I wouldn't lead. My eyes swelled in tears as I took my last breaths. Then everything went black.

It seemed instantly that I was somewhere else. It was as though I stepped through a doorway in the back of my mind and into another dimension. I first noticed the floor or rather the absence of it. There was a thick mist or fog that covered the ground up to my knees. The very next thing that I noticed was the sound of music. It soothed me. I didn't know the song but it seemed familiar. It was symphonically orchestral and with brass, strings and woodwinds, but the sound was still subtle. The sound seemed to be coming from my right. So, I looked in that direction.

I saw a deep void of darkness, like an astronaut who looked into the stars from orbit. Then I looked in front of me and saw my deceased grandmother. She was standing just in front of the 'White Light.' The light radiated warmth, light, love and anything I needed to know. I also noticed other figures off to the left of me. They seemed peaceful in pairs holding each other and swaying with the music. My grandmother delivered the choice to me. I could stay with her or go back to my life. She told me that if I stayed, everything would be o.k. She said that if I went back to my body, it would be the most challenging experience I would ever endure. Then she showed me my grieving family. It was like I was transported to the moment when my loved ones were talking about me. I saw my other grandparent who were still alive. She was driving to the hospital to see me. My Papa was consoling my grandma saying, 'Don't cry, he's going to make it!' I also saw my best friends driving to see me at the hospital. They were saying how bad it was and how I didn't look like myself. I also saw my mother and aunt in the hospital watching over me. I saw their tears and the uncertainty in there expressions.

The night of the car accident there was a friend of my Aunt whom I had never met. She woke my Aunt up in the middle of the night to say that a young man my Aunt knew was in terrible danger. This friend of my Aunt happened to be a sensitive. My Aunt called my Mother right afterwards. My Mother had just got off the phone with the sheriff's office. My Mother had seen a vision of her mother and I laughing and having a good time. This was during the same time as my NDE. I also had a premonition dream three years before the car accident.
More Information:

Dr. Jeff: I would appreciate any comments that you have to a question of mine. One of the questions that you responded to, and your response was:

Did you have a sense of knowing special knowledge or purpose? Yes I felt as though just seeing the crossroads and standing before the light gave me an experience that not many other people had. My grandmother conveyed a piece of information regarding everyone, that NDE's and experience like them will become more frequent and people will begin to see the true 'Meaning of life' That is to LOVE give it receive it share it and spread it in any way you feel.

That is very interesting that you became aware that NDE’s and experiences like them will become more frequent. With regard to NDEs, did you have a sense as to whether this meant more people would have life-threatening events resulting in more NDEs occurring, or could this mean there will be about the same number of life-threatening events with more people will having NDE-type experience at the time of the life-threatening event? Any further comments that you have on this would be appreciated!

Thanks again for sharing!

Best regards,

-Jeffrey

William: Thank you Jeffrey, to answer your question. When my grandmother conveyed to me that more people would have experiences that would open their eyes to the dimensions that exist parallel to ours and how strong those dimensions influence ours. This did not necessarily mean that people would have to suffer physical pain in order to come to that point of understanding. She inferred that every person would come to that understanding in their own way. Basically she told me about more of a spiritual awakening a shift in the paradigm of the collective consciousness of our dimension. I personally believe this is related to the changing of the ages and our solar system's position in our galaxy.

