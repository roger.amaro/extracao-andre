
I am a runner and had won in my age group for four consecutive years.

After running a 5k race in the Smoky Mountains, I was driving with my wife back to the mountain house. She wanted to go by the farmers market on the way back. As I started to go down a little street, she insisted that I use a different road. As I steered the car, my shoulders started to cramp, and then came a massive chest cramp. I said, 'Something's really wrong.' I went into full cardiac arrest while driving. She put the car in park and started to scream for help.

In the farmers market, where two sisters were hiking, there was a nurse practitioner and a doctor buying vegetables!

I was dead on the ground for 10 minutes while they worked on me. Two ambulances showed up and shocked me ten or 12 times trying to get my heart to beat.

During this time, I was in a tunnel surrounded by beautiful clouds with a light in the distance. I could see a figure of someone in the mist. Then I heard a soft, kind voice that told me to go back.

They sent me to the hospital where I remained in a coma for four days. When I woke up, the started asking me questions while pulling out the life support tubes. In the room I saw my family and a bunch of doctors. One doctor said that I was a miracle and that someone was watching out for me. He said that I should not have made it because it was the widow-maker blockage.
