



I had a major heart attack on Friday the 13th of May 2016. It was the worst kind you can have. I tried to call relatives who were in the house for help. They thought I was over exaggerating. One of them joked, 'I feel like I'm having a heart attack too!' Since they wouldn't take me seriously, I tried to take a shower. By the time I was done, I knew something was terribly wrong, so I stumbled into my bedroom and called 911. I don't remember getting dressed, talking to the dispatcher, or walking down three flights of concrete steps. When I got to the bottom of the stairs, I was relieved to see I had clothes on. But I had ripped a fingernail off. The paramedics pulled up as soon as I arrived at the bottom of the stairs. The paramedic calmed me down. I started to go unconscious and the paramedic was asking me what my pain level was. I tried to answer him and distinctly remember saying 'Seven on a scale of 1 - 10.' He responded, 'OK stay with me.' I was confused as to why he didn't hear me. At this point, I knew that I was dying and got very scared.

Then BOOM! I was surrounded by a beautiful, white light! I was wondering about my beloved dog Blue who had died just 10 days before this event. He spoke to me telepathically and in English. It was very comforting to know he was O.K. I than had like a whoosh of sudden information like an instant download of information. The information was about all my loved ones and so much more that I can't remember it all.

Than I met my best friend who I had wanted to see for so long. It was Jesus! I just knew it. Even though he didn't say my name, I didn't ask his either. It was like we always knew each other and he loved me so much! All the information and communication we had was telepathic and instantaneous. I do remember arguing with him at the end, not in a bad way, but saying, 'Please don't send me back there!' I audibly heard his voice and I'll never forget his real voice. He said, 'No, not yet.'

I still cry when I think of this experience.


