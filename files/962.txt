
I remember standing next to the emergency room doctor who was screaming at several people while they shocked me over and over and over again. He wouldn't give up. I kept trying to tell him that I was okay and had never felt better. I wanted him to understand. But, the feeling that I had was that I understood everything (is as close as I can describe) and that this was much better.

I slowly let go of all the pain and sorrow and yelling in that emergency room. I left that emergency room for a little while and I'm not sure what happened but I remember a very old man touching my hand and I was back in my body. I was on a ventilator when I woke up and then I developed pneumonia. After 2 or 3 days, the nurses made me get up and walk in the hallway. I saw the Doctor who had been screaming at everybody and wouldn't give up on me. I went up to him and thanked him for saving my life. All the blood drained out of his face and I think we both realized, at the same time, that we had never met when I was alive.

I was unconscious when a man in a dream told me he would take care of him for me. I had this warm, cozy feeling that everything was alright and when I awoke I was so happy.
