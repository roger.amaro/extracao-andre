
I could hear the voices of the doctors and then they just faded into a lower volume and they were muffled.

I then felt the doctors begin cardiopulmonary resuscitation on me but it was like I was standing in a room and someone was banging on the floor causing a sound above me.

I could hear my heartbeat slowing down and it slowed down a lot.

I then felt myself begin to fall and I just kept falling and falling. I felt calm at the time. I wasn't scared. I was calm like I was falling through a tunnel, a never ending tunnel.

Then it felt like I exploded and I could hear the doctors around me again. I was still unconscious.
