

I remember rising up over my body and looking around the ICU room. There were frantic doctors who were confused about what to do.

I saw my maternal grandfather yelling at the doctors and telling them what to do. He had passed over a while ago. He appeared to be about 35 years old, which was much younger than I remember him. Than somewhat slowly the room and then the earth moved away from me. I distinctly remember that there were no stars in the space around the earth and clouds on the earth.

I seemed to stop moving away at some point and became aware of an unknown source of great energy behind me. I could feel it against my back. I remember thinking about whether I should turn around and face the energy. But I had young children who needed a father, so I considered that as well. In my mind, I had a choice to stay here and enter the energy knowing there could be no return if i did. Or I could return and raise my kids. The very instant I decided I wanted to go to raise my kids, I was back in my body.

I do not fear death any longer. I feel that I was given the gift of empathy to a much greater degree than I ever experienced before. I am permanently changed. My wife also agrees that the event profoundly changed me. She says that 'I see people' now for who they really are. I agree with that assessment, but I had to learn to trust it.

