

I had lymph node cancer. During chemotherapy, I had many thromboses in all possible body parts. Beforehand, I already had massive headaches, which were leading to attacks of sweating and vomiting when standing upright or sitting. I also had a reduction of my field of vision. The doctors subsequently diagnosed that I had metastases in my brain.

Afterwards, it turned out that it was a sinus thrombosis. My sinus thrombosis was a pivot point in my cancer therapy, as I had a stroke, went into a coma and kept having seizures. I was transferred to the neurological hospital, since the doctors thought I had tumors in my head which needed to come out.

In my coma, they didn't give me any painkillers since I had no reactions to pain stimuli anymore. A short time later, I got a very high fever 41°C (106°F) and together with the pain, I had a feeling like being in a frying pan. I suffered a lot from the fever and pain. I heard unbelievably loud 'praying' voices that seemed to pray in a language unknown to me. Over and again I heard the same 'words' in an unknown language. I, nevertheless, understood their meaning somehow deep within myself; with a sound intensity that almost seemed to blow-up my head.

The fever, the unbelievable pain and this extremely loud, repetitive praying, have been so unbearable that I only wanted to die.I wanted to leave my body.

*Whoosh*

I was outside of my body. It happened very fast and easily. I was lying in my hospital bed with a nurse on one side and both of my parents on the other side.

I felt infinite relief, no pain and no fever anymore. The praying had stopped too. It was calm and peaceful. I enjoyed this tremendously, it was so nice. I didn't feel any fear at all. Everything was as it should be and the way I wanted it to be!

I had no fear of the loss of my body. I didn't have a feeling for my parents, who I knew were so sad when I'm dying. All this was unimportant for me. Something was pulling me away from my hospital room, out through the ceiling, and away from this world.

In the next moment, I was in a park with birds chirping and insects humming around. It was very beautiful. There were flowers everywhere and various types of grass and great, old trees. A mild breeze moved the leaves making them rustle. It was a pleasant day in late summer at an advanced afternoon time. The sun was shining, but it wasn't unpleasant. I looked at a small house that seemed to fit into this situation. A tall, elderly man was in front of the house and was wearing a dark coat/hooded cloak. He had a long beard, looked at me with loving eyes, and welcomed me with caring words.

Despite the fact that this situation was so infinitely peaceful and beautiful, I had a feeling that I didn't belong here. I didn't feel RIGHT at this place; it was very surreal.

I greeted the old man and thanked him for welcoming me. I told him that I felt wrong here and something was not right. I suddenly was unsure if this was the right decision to take, as I only had wanted to get away from the bodily pain, fever and unspeakably loud praying.

He felt my uncertainty and revealed to me that now I would see a movie, showing me all the things that would not happen if I left my life now.

Everything seemed to tumble down upon me at once. I saw myself giving a lecture in front of health professionals, saw myself writing reports and giving lectures at college. I was involved with discovering new possibilities and ways to help sick people find a different perception of the NEW MEDICINE. I saw myself standing on the dock of a worldly court room, from which I would get away with an acquittal, about problems that the Pharma lobby would cause me.

In the end, I would die at an old age of a natural death. I would pass away peacefully among my loved ones in an armchair. I was immediately clear to me that all these situations NEEDED to happen. All this had to happen like this and not differently.

I said, 'All this has to happen. I have to go back can't stay anymore.' The old man nodded knowingly and said, 'So be it!'

I then told him that I had no strength anymore, that my body was so degraded due to chemotherapy and radiation, due to space food and artificial food, due to fever and coma, that I had used up all my resources, that I was just a skeleton, and that I was all skin and bones. 'I can't manage this alone. I can't keep going', I said. 'PLEASE HELP ME!'

*Whoosh*

I was back in my body. Everything was fuzzy, soft, warm, and fluffy; like cotton wool and clouds. I had no fever, felt no pain and it was calm around me. I knew I was recovering. Now I could sleep comforted. I fell asleep deep inside of me and in my coma, I was calm and content. I have almost no memories and no sense of time.

I slowly woke up from my coma. The medical personnel told me later, that I was looking at them with very big eyes; they said I had 'speaking' eyes. They tried to get in contact with me using the 'Zwinger' procedure to get an idea of my state of mind. I couldn't move my body, only my head a little bit. I had forgotten how to speak. I had aphasia of speech. Now, it is more than 25 years later and I'm sitting, fully functional at my laptop and am filling out this form.


