

A year before my experience, my brother had passed away. The evening before the heart attack I got severe back pain in the shoulder area. I disregarded the pain, thinking that I been carrying too heavy of loads. In the morning, I still had the back pain. I sent my son to school. He should have passed by my in-laws which was two houses over from our house. Our youngest son was still sleeping. Then I remember vomiting and screaming aloud. My in-laws called the emergency doctor. I was lying on the floor in the living room. The next thing I remember was leaning against the table while I watched two doctors trying to reanimate me. I kept thinking, 'You can't go now. You can't do this to mum. Last year Achim (my brother) and now me? I can't do this to mum.' That's the last thing I remember.

