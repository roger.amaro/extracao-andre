
In December of 1979, I was attending a National Guard weekend drill. Went to eat lunch and once the food went into my stomach I became over taken by pain all over. I went to the bathroom to empty out and I could not release the pain. I had wanted a co-worker to take me to hospital but she had a date to see her boyfriend. I could not get a hold of my boyfriend who was cheating on me with another girl in my car.

A co-worker dropped me off at my home where I lived with my father and mother. When I entered my home no one was there. I wanted to call 911 but I could not get the breath out so I crawled up to my bedroom so I could die on my bed. For whatever reason my dad, who never ever went into my bedroom, had a prompting to check on me in my room when he arrived at home with my stepmom (mother). When my dad entered my bedroom he thought I was having a heart attack because I was holding my heart and could not speak very loud.

My father rushed me to emergency after finding me on my bed; pale and having a difficult time breathing. He carried me to the car and rushed me into the emergency room right away. I was going in and out of consciousness. When I arrived at the hospital, they were not sure what the problem was. I was misdiagnosed at first because I was young and strong and still wearing my National Guard uniform at the time in the emergency room. We are more advance with medical equipment in this day and age. A new doctor examined me and said he would do an exploratory laparoscopy procedure.

I had a cyst on each ovary and they both had ruptured so I was slowly bleeding inside my abdomen. I was bleeding inside the whole day until I ate and it moved my blood around in my abdomen and that is when the overwhelming pain began and lasted until I fell in the bathroom in the emergency room right before emergency surgery.

The emergency room was a very cold and unkind place. As a staff member was wheeling me into surgery, I asked to go to the bathroom. When I entered the bathroom I do not ever remember returning to the stretcher. I do remember falling in the bathroom.

That is when I went through the most healing light. It was warm, loving, and peaceful. I was surrounded by spirits all around me. I was so happy not to feel any more pain both physically and emotionally. I knew it was another realm and not here on earth. I had lost 1600 cc of blood. The doctor was excited that he saved my life.

The next day when I woke up I was depressed to find myself back in my body. I was depressed when I realized I was sent back to this world.

After a few months I started to get dreams that came true usually within 3 days. One young lady told me because 3 is a powerful number in the Bible. If it is after three days I chalk it up to coffee or a negative movie.

I hope this helps. With all that was against me that day I knew without a doubt it was a higher power that wanted its plans accomplished. It all seemed choreographed. I could not find a ride to the hospital, my father never ever entered my bedroom and I will never know what drew my father to my room that day. I could not speak loud enough to call 911. My boyfriend was not reachable because he was out without another girl. Finally it was not my will to return.

After my experience, I went to school to be a medical assistant. I am now a certified surgical tech. I work for the doctor who saved my life. Blessings and love Dolly
