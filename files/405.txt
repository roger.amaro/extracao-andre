
I was 3 years old when I suffered from severe pneumonia. My memories of this NDE became clearer when I got older. The only thing that doesn’t happen anymore is the feeling of pure happiness when the NDE flashes come to me.

I was hospitalized. I remember seeing the hospital from above. Soon after, I was floating on my uncle's farm. My vision was 360 degrees. I remember being ashamed because I was naked, but in spirit and without a body. I saw a pile of shingles piled up at one end of the road. I remember running through a wonderfully beautiful garden and soft grass like cotton. Somebody inside of a house was taking care of me. The love I felt was so intense that I can never describe it with words. I think it was my maternal grandmother, but I’m not sure.

I survived. Relatives told me that I was in a coma for days.
