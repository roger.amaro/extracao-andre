

I have health problems due to diabetes and lost feeling in my feet. Consequently, when a spike went through my shoe, I didn't feel the pain in my foot. When I realised that I had an infection in my foot, I went to the doctor. They left me in the hospital for a few days. During this time, they decided to do surgery so I would have feeling in my foot. It was an operation without much risk. They took me to the operating room and gave me general anesthesia.

When I was under, I went for a moment to another world without realising I was taken to another place. This world had many colors, and everything was so bright. I was able to see myself from every angle. I saw there were many tunnels in different colors. They were like interdimensional doors to other places. Everything was surrounded with colors, or more accurately surrounded by colors that were like stained-glass windows. Everything was very beautiful.

I chose to go into one of the tunnels. In an instant, it took me to somewhere. It carried me so quickly that I felt it was in the blink of an eye. Where I went to, there was a unique type of giant temple that was so big that the people looked like ants. I could see someone in the distance, but I didn't know who it was or the memory has been erased.

All of a sudden, I was back in the operating room and a nurse was asking me if I was O.K.

