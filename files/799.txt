
We were at the top of the stairs, my older brother, my older sister, and me. My brother as usual was heckling us. My mother was occupied downstairs in the kitchen, and didn't stop telling us to stop messing around. Suddenly, my brother pushed me, I lost my balance and fell.

I saw myself cascading down the stairs like a rag doll. I was out of my body and floating above the stairs. I saw my brother, sister, and mother screaming while taking me in her arms at the bottom of the stairs. Meanwhile, I was up above and didn't understand a thing. She was screaming at them and crying for me, without me understanding anything as I was above them and could see everything. I remember sitting at the top step of the stairs, then I was sleeping, and then, still without understanding anything, I was reintegrated into my body.

I told my mother that I was okay and that I saw everything from above steps. She never believed me because I wasn't hurt. She returned to work.

I never forgot that moment. Did it change the course of my life? Yes, for sure. At the age of three years old, so many people tell me that they have no memories whatsoever of their childhood. While for me the memories still are very present as if I would still live in that time. I was only three years old, didn't go to school yet, and I only went to the square with my mother.

A second phenomenon during my fourth pregnancy leaves me questioning when I found myself in reanimation. I have difficulties speaking about that, not being sure anymore concerning the external vision of what others might think of it.
