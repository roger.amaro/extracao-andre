

It’s dark but I can hear intermittent cars going by, so I’m guessing it’s about 4:00 am. I feel comfortable and although I am usually awake at this time, the baby is asleep and maybe I can sleep for an hour more. So, I give it a shot. I turn over and suddenly feel a hot rush in my pants, the familiar feeling of blood overtaking my feminine products. Wait! I’m pregnant. My mind suddenly snaps into reality-focus. I get up quickly, run into the bathroom, drop my blood soaked pajama bottoms to my ankles and sit on the toilet, blood incessantly spraying out. I yell for my husband. He runs into the bathroom, takes a quick look at the situation. He has terror in his eyes. I ask him to get my cell phone, I need to call 911. Frantically, he says that he will call but I insist on him getting my cellphone. He does and I make the call. Meanwhile, he is on the house phone calling 911, himself.

'911, what’s your emergency?' the calm woman says.

'I need an ambulance now! I am pregnant and I am spraying too much blood.'

'Ma’am, can you see the baby’s head?'

'No, and I’m not going to! I have Placenta Previa! The baby has no exit! Please send an ambulance NOW!

'How far along are you, ma’am?

'About six and half months. Please! Please! There’s no time for this I need an ambulance!'

'Ma’am, please look and let me know if you can see the baby’s head.'

'Lady! I told you, the baby has no exit. This delivery will be a C-section no matter what. This is not okay, it looks like a murder scene in my bathroom! Too much blood! It’s not stopping! Please stop asking questions and send an ambulance NOW!'

She asks my name and address, my husband’s name and I frantically give her all of the information.

'They’re on their way ma’am. I’ve just been informed that Lane is talking to us right now. You need to hang up and go lay down until they arrive.'

Lane is on the house phone and I can’t make out what he is saying. He helps me up and half carries me to the living room couch. I can see a trail of blood on the carpet. I sit down not knowing what to do. He is on the phone pacing and orders me to lie down. I don’t want to. I need to ensure my situation is understood when they arrive. I am so dizzy and struggling to remain upright. My eyes are so heavy. I can hear the sirens coming. Then everything goes dark and silent. It felt like an eternity has passed before I wake and notice men jostling my body. I see my husband across the room talking to two men in uniform. I’m not sure if they’re police or fire department uniforms but I feel worried that my house is so messy and my living room is crowded with several unknown men in various uniforms.

'We are going to lift you, June, Don’t fight us. We need to put something under you. You are bleeding a lot.'

I feel my body being pulled to the side then pushed to the other side as they slide a thick plastic or vinyl feeling thing under my whole body. In my mind, it felt like a body bag. Nothing is making sense. I couldn’t move my body or speak. I felt like a stranger witnessing everything from someone else’s eyes. I couldn’t voluntarily move my body and I felt detached from myself.

'Ready? 1 – 2 – 3!'

I feel my body cradled in the plastic cocoon and the weight of my body suspended as the couch leaves me. They set me down, square on my back. I was placed on a very narrow plank with straps being secured around me. I feel like the plank is so narrow, that I might fall off. The straps do not feel sturdy enough to hold me. With a sudden jolt, I am elevated and the plank begins to move quickly through the living room and past the TV toward the front door. There is a lot of yelling amongst the men but I can’t understand anything. The ride is very bumpy as they move me across the threshold of the front door, down the concrete step, over the grass and down the driveway. I open my eyes and see the stars in the sky.

'June! Stay awake, I need to talk to you!' the stranger demands.

I look to the side to see my street full of police cars, fire engines, and the ambulance. My eyes are fix on the back doors of the ambulance before I am spun around to see the police cars and fire engines once again. I look up at the stars again before I am lifted into the ambulance. The beautiful sky is overtaken by the harsh, artificial lights in my face before everything goes black and silent.

In the blackness, I am weightless, timeless, and ageless. I can’t feel anything. There is no gravity, no breath, no sound, no attachment, no body, no temperature, no cares, and nothing at all. It’s a feeling of stillness but with a tunnel or tubular vortex propelling me. Still, there is no concept of movement. I just know that I am moving toward something or someplace I desire to be. It is such a profound feeling: A feeling of pure blackened silence and total bliss. There is no fear here, no anxiety, and no expectation. I feel a deep, unearthly love holding me that is so familiar and is a feeling I have longed for my entire life. It felt like I finally arrived home after a miserable journey. I was tired and it is a well deserved rest. I am lost in pure, loving bliss and gratitude for being present here. It could have been one second or a million years. There is no time in in this place of profound purity.

'June! June! Stay with me! I need to know how far along you are!'

The loving ecstasy is grotesquely ripped away by the glaring lights in my face, the sound of sirens, and the creaking noises from the moving ambulance. I see darting movements over and around me by the men. I feel offended by the seeming warzone by comparison to where I just was. I wished it would all go away.

I see myself below. For the first time, I see a sad but beautiful woman. I watched as a hairy arm that was connected to a crisp white, short-sleeve reaches across her body and with a light blue gloved hand, presses a clear mask over her nose and mouth. He reaches behind him with the other gloved-hand and turns a tank dial. The other man pulls her arm out, yanks her shirt-sleeve up and jams a small device into her arm and covers the unknown thing with tape. I see the driver look back and shouting, but I can’t understand. The men are shouting back at him but none of it matters. I don’t seem to have a care now. She looks so peaceful in this moment. I felt so much love and respect for her as I voluntarily moved back into the profound void.

My head is violently shaken, and again, there are offensive lights and noises.

'JUNE! JUNE! WHAT’S YOUR NAME?!'

I looked up at the man who genuinely looked like he needed this information like his life depended on it. Although he appeared out of focus and almost vaporous. I didn’t even bother answering him. It was too much effort to form words and he just said my name anyway.

'ARE YOU GOING TO HAVE A BOY OR A GIRL?'

'gerrrrlll'

'What’s her name going to be?'

'No. It’s a secret'

'I promise to keep the secret, please tell me her name!'

'Morgan'

'Morgan needs you to take deep breaths and look at me RIGHT NOW!'

How did he get this job? It took so much effort to speak. Didn’t he understand that? Forming words and remembering details was close to impossible. I wished he would just leave me alone and let me go back to the void. He didn’t seem real. He seemed like a trick of my mind. I needed to return to the loving familiar space.

'You have a baby at home! It’s it a boy or girl?'

'His name is Liam.'

'June, you have to stay awake and talk to me! Your babies need you to keep talking.'

I will myself to return to my long lost loving paradise but again, I see myself below and this time I have more awareness.

'She’s not fighting.'

'Hey Ma’am! You have a son at home that needs you to take care of him. Liam will want to see you when he wakes up this morning. Your husband can’t take care of him by himself. MA’AM! IF YOU GIVE UP, YOUR DAUGHTER WILL DIE!'

'JUNE, JUNE LOOK AT ME!'

As I witness this scene, I see her lifeless body, the mask obscuring her face now and I realize the knowing of needing to decide what’s best for her. I see the men on either side of her fidgeting with her arms, and giving her injections, the driver keeps glancing back. The three men communicating with one another, and shouting at the girl on the gurney, who just appeared to be a lifeless object. I didn’t feel any urgency. I was disconnected as though I was watching a movie where you have a fondness for the protagonist but an acceptance that this is the final scene. I just knew that everything was fine no matter the outcome.

I return home again and let the love drench me. I savor it and feel endless gratitude for this place. I picked up where I left off, in a blackened tunnel. No fear, no anticipation, no senses. Just a knowing. I was content to be in this familiar endless moment. I am aware of the gratitude I feel for being here. I continue to be pulled toward something like a magnet. Aware that I am moving toward somewhere I long to return to, there is a strong awareness that I am being accompanied to the destination by multiple unseen beings. Where this knowing came from, I am not aware, it was just a definite knowing.

I feel a sense that it is a choice I am free to make. I allow myself to drift in the vortex with a sense of pushing the boundary to the event horizon. I become aware of the knowledge that I am a warrior, of sorts, a Being meant to make others uncomfortable and assist in a needed change but a feeling of needing the answer. I don’t know what I am supposed to do or how. I understand my strength and my willingness to always do what I feel in my heart to be correct no matter the compromise or sorrow it may cause me, even if it leaves me isolated.

I am consumed with the feeling of love and that it is something I am destined to do but the relief of being here is so enticing. I become aware of the sense that I will return again sooner than I realize and the compromise I make now will be worth the sacrifice of this unexplainable moment. I have a knowing that going back is a determination I made for myself long before now. I feel profound sadness as I consciously decide to go back to my miserable life.

I feel a loving send-off and the sense that these presences are giving me a sort of inspiration that I can accomplish what I am supposed to alone. I feel that I can do it. A knowing that I chose to do this already and that I didn’t finish. The presences’ give me a strong feeling of validation and that I will remember that even if I am alone, they will be waiting for me when I complete the mission. They fill me with love and encouragement as I consciously agree that it won’t take too long and I’ll be back. I accept that I must return to the ambulance not because I want to but because I am obligated to for some purpose.

I open my tearful eyes and my senses are overwhelmed with the assault of human inventions. Lights, noise, abrupt movement and restraining devices on my body and face. I am offended and I feel cheated and angry for the situation. As I notice the blissful enchantment quickly fading, I fight my memory to hang on to it and never forget it.

In that moment, I realized that I had been there before. The feeling of a pure unearthly love and zero attachment to anything, anyone or even a body for that matter. It was familiar but I had no memory of it before then. It was the feeling that was familiar. I relived it in my mind as the siren shut off and after a series of some turns and a backward motion, the ambulance stopped and the doors flung open to reveal a group of uniformed strangers pulling my wheeled plank towards them. I swear to myself that I will leave my comforts and change the world.

I think of the man that I have been in love with for so many years and how I never had the chance to experience life with him. I think of the places in the world that I have always wanted to visit and the unwritten music from unknown artists that I have not yet internalized. Maybe the music I haven’t written yet? I think of my Liam and long to hold his future children. I think of my unborn daughter and her life now hanging in the balance. I fear that I am going to lose her and decide that I will do whatever it takes to look into her beautiful eyes and tell her this story someday. The thought of Lane didn’t cross my mind when considering my future. It was the man I couldn’t have that I saw in my future, my three children, and beautiful things I have yet to accomplish and experience. I was ready to live but profoundly saddened to have left the love and serenity of where I just was. I felt love-drunk and sentimental during this fleeting moment but it would pass quicker than it came.

I was hospitalized for a month before I had another bleed-out followed by an emergency C-Section. In that month, I did a lot of life reflection and evaluation. I revisited the memory or what had happened over and over again. I made the decision to make major changes in my life. Simplify, and terminate superficial friendships and belongings. My daughter was born prematurely but healthy. I recovered just fine.

My daughter is 12 years old now, my son is 14. I divorced my husband three years after the incident. I did finally have a relationship with the man I always loved but it didn't work out. To this day, I am still trying to wrap my head around the experience and figure out what I am supposed to do in this existence.


