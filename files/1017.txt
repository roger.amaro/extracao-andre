
I was four years old. I fell and injured my head. I lost consciousness. Then, I was above myself, aware of the injured child. I connected to the injured me in a way I cannot explain. I was older yet aware of both of us. I could see from both views.
