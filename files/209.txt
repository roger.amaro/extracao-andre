

The accident itself was portrayed in that old TV show Rescue 911.

My vision flickered when it went into tunnel vision twice, real quick. Then I went unconscious.

I found myself suspended on my back with my arms and legs crossed as if I were lying in a coffin. Then a wave of bright, white light came over me. It was a bright light. I could still feel discomfort and I was clammy. Then another brilliant white light came over me and everything was released. I was in a state of suspended animation and bliss.Then a brilliant, white hand came over my body. I felt the hand masculine. I was amazed at how bright, calm and euphoric I felt. I marveled at how clear the fingerprint lines were at the bottom of the hand's fingers to the palms. Before I could ask a why everything worked the way it did, it was answered. I remember the answer was so simple. Then I heard a voice through my body say, 'You're all right, I like you.' The hand was lifted and the first bright light was back.

Then I could feel pain again and I was in the arms of a deputy Sheriff calling my name.


