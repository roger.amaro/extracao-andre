
I can remember my evening being uneventful and going to bed at the same time as my husband. He was to my left and sitting up reading. I was lying in bed (sleeping) when all of a sudden I woke with excruciating chest pain on my left side. I realized instantly that I was completely paralyzed in bed. I was wide awake but could not move, not even blink. I can remember lying there screaming on the inside because of the pain and trying to raise my left arm to touch my husband. I can remember screaming in my head that I needed a doctor. I felt that if I could just make my arm move to touch him he could call the doctor. The pain in my chest was so excruciating.

The next thing I remember is that I was hovering above the bed, way above it, higher than the ceiling. The room was filled with a bright, bright light with a pinkish hue. The light was brightest above me.

I was floating. I could feel my whole body floating, not a feeling like an image, but actually floating like in water. I was looking down and I could see myself lying flat on my back and my husband still sitting up in the same position. He hadn't moved and I can remember thinking how on earth could he be so nonobservant with all this going on!

I remember continuing to look down and hearing my mum's voice above me. She had passed away in March of that year. She said, ‘it's not time yet, you have to go back for Gareth and Damian.’ (My two sons who were young but not living with me.)Those were her exact words. I remember them clearly to this day.

Next thing I remember I was back in my body and awake. My pulse was racing, my left arm was aching, and I was sweating. I told my husband what had just happened. He shrugged it off.

I did tell a colleague at work of this as her husband was a Pastor. She discussed it with her husband and told me various religious scenarios. I can’t really remember her responses now but to this day I have thought of this episode many, many times. My recollection of it in its entirety is still in color and accurate. No details have diminished over the years.
