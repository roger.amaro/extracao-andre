
I awoke early in the morning with severe chest pain and asked my wife to call 911. I got up to go to the bathroom and passed out on the toilet, falling to the floor. My wife said that I was out for about a minute.

Before awakening, I experienced the presence of unrecognized beings that were comforting me without any physical contact. I just felt they were close. I could not recognize my environment but I was lying down in a warm and well-lit place. I had a sense of ineffable, inexhaustibly infinite peace and well-being. I had the sense of being totally loved and accepted. I had absolutely no fear, which was a unique experience because I have a chronic anxiety disorder. A transient experience at most seemed like it lasted a few seconds.

I did not resist waking up but felt deeply nostalgic for the experience almost immediately and have had the event in my mind since that time. I can't shake it. After the event, I was taken to a hospital emergency room, was admitted, and had coronary artery interventional catheterization, because of a complete blockage of my right coronary artery.
