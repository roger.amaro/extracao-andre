

As a boy of about 13, I was standing on a bridge that was also a dam. I was leaning against an aluminum, light pole that got hit by a bolt of lightning. It knocked me out. My step-dad saw it and tried to revive me by by CPR and heat massage. As I lay there on the road, I was in a deep, dark place that was absolutely inky black. I couldn't see anything. After what seemed like a minute, the blackness gave way to a gray, and then there was a white cloud. Then there was absolute clearness, as if I was floating in space. Then I began to travel at a really fast speed, yet I felt no wind or sound.

All the stars seemed to blur as the speed increased in a tunnel of light. At the end of the tunnel, I saw a bright pin-point of light. As I got closer, the light got bigger until I entered the light. I found myself in a beautiful realm that had mountains and very green grass and the bluest sky.

I saw a small, winding creek about a hundred yards in front of me. It sparkled as it reflected the sunlight. The water was clear as liquid air, so clean looking.

In the distance, I saw a tree that was about 400 to 500 feet high, but probably closer to 500 yards away. The tree had many colors to it's leaves, like gold, green, brown, and yellow. The tree sparkled like it had lots of diamonds on it. With a blink of my eye, I was next to this tree within a second. I was looking at each leaf and could see what was reflecting all the dazzling little colors of light. There were raindrops reflecting the the rainbow within each drop of each of the colored leaves the raindrops rested upon.

As I looked back in awe at the distance I traveled, I saw a rocky outcrop of large stones to my left. Instantly, I knew somebody important was waiting for me there. I also saw some figures of people far in the distance who were wearing brightly fluorescence colors. I again turned my attention to the rocky outcrop and proceed towards it. Within moments, I was there. I saw a girl of about 18 or 19 years old, that I knew was my soul mate. She saw me at the same time I saw her. I saw a look of surprise on her face. It was then that I realized I had just left this place and had returned after only a few minutes. Yet by my earth time, I had been gone the 13 years of my age. It was confusing. My soulmate was dressed very differently than the other Beings I'd seen in the distance. She was wearing a green outfit like you would think Robin Hood would be wearing. She also had very short hair, like a boy, and her skin was almost completely alabaster white, like marble.

As I approached her, she seemed to be telling me that it was not yet my time and that I couldn't stay. I remember pleading with her and crying to let me stay, but she said I had things to 'witness' and then I could come back and stay as long as I wanted too. With that, she waved her hand and I was drawn back into the same tunnel of light I had come through. You would think this is the end of the story but it isn't, it's only beginning!

As I returned to where my body lay, I saw my stepdad doing CPR and trying to revive me. Even when he was giving mouth-to-mouth resuscitation, I saw a bit of steam exit my mouth. Within an instant, I was pulled back into my body. As I opened my eyes, I developed a severe hatred of my stepdad for bringing me back. I blamed him for my return. Although this would later wear off and I realized he was just trying to save my life. I stayed in bed for about 3 weeks because I was too severely sore to move. We didn't have money to take me to the hospital at the time.

I never told my Mom or stepdad what I experienced. I listened to a great many so-called experts who said that people who experienced this kind of situation were suffering from lack of oxygen as the body shuts down and the brain starts to suffocate. I was confused for a great many years thereafter. I wanted to believe it was real. Then, one day I was watching a TV show on NDEs. They were having a group discussion by people who had been declared dead but lived. As they were going from person to person, they came to a guy who said he too had been electrocuted. He had been adjusting his roof antenna as it touched a power line and the electricity had knocked him off the roof.

As I sat there listening to him, he started saying everything that I had experienced 17 years earlier. He said everything in the same order that I did and described exactly what I saw! He saw the grass that was so green and the sky that was so beautiful. He talked about the crystal-clear, babbling brook that shimmered in the sunlight. He saw the tree with all the multi-colored leaves shimmering like sparkling glitter. He said he saw the sunlight sparkling through each raindrop on each leaf. He talked about seeing a young teenage girl of about 19 or 20 years old who was sitting on a bunch of large stones off to his left. She also was wearing something like a green Tinker Bell outfit (much like what I described as a Robin Hood outfit). He said the girl told him it was not his time yet and that he must return until his it was time for him to come back.

I almost lost my breath, because there were too many similarities for this to be a coincidence. I knew then that what I experienced had been real.

My life changed forever at that point. I knew that there was life after death, that there was a heaven; that there must be a God in heaven and that Jesus Christ was real.

To this day, I often profess my love of our father in heaven and accept Christ as my savior.

With this said, I share this story with all.

Love God, Love Christ

Amen.

Johnny



