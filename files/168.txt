

I was in the hospital to deliver my baby. Labor was induced because I was ten days overdue and showing signs of pre-eclampsia. I gave birth to a beautiful baby boy, but about an hour later, I became very unwell. I developed eclampsia which is abnormal after delivery. I had terrible chest pains and started to have a seizure. There were several doctors and nurses working on me. I was in terrific pain!

Suddenly, I became aware of a tunnel. I started to move towards it, almost floating. I saw the most beautiful light and was being beckoned by someone or something. I knew instinctively that I was dying. I told the nurse 'I’m going to the light.' She panicked and kept shouting at me, 'Stay with me! Don’t go to the light!' I started to make a decision whether I should stay because my baby boy needed me or if I should go into the tunnel of light. I felt rational and knew what choices I had. I got to the tunnel and really wanted to go into the light. I felt guilty that I would consider going to the light over being with my beloved baby. I still feel guilty about that even now. Any parent will know how massive a mother's love and sense of protection is for a newborn infant, but the light and love was even more intense than that feeling. I can’t honestly say what made me return and I’m not sure whether I had a choice. All I know is that I still had job to do and that was nurture my baby boy.

The nurse came to visit me five days later, since I had been in an induced coma in Intensive care. She said, 'You totally freaked us out, as we thought we’d lost you. The nurses tell me that most people who talk about going into the light die. Do you remember it all?' I said, 'I remembered it all.'

This experience has released me from any fear of dying. It was a beautiful experience and not frightening. I went back to school into nursing soon after and believe this was due to this experience and wanting to help others. I’m still trying to enhance my spirituality. I kept my NDE a secret for 30 years because I believed people would think I was crazy or was hallucinating at the time. I’m so blessed to have had this happen to me and want to share it with others.


