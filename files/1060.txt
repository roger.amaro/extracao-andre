:

Due to complications from a medical procedure, I had a near death experience. I could see my body on the operating table. I could also see and hear the medical staff, but was not interested in what they were saying. I was surrounded by God and bright white light. God comforted me and invited me to heaven. God also gave me the choice to come back to life to raise my three year old son. Since, I was a single mom, I am glad I made the choice to return.

My son graduated from Harvard and has a great job. I have had the opportunity to enjoy God's creation and live in a beautiful home. I have shared some wonderful times with family and friends. I have also read the bible and recently became a grandma.

