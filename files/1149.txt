
In 1960, I was 6 years old and drowning. I saw the events of my life passing rapidly. I had the impression of going through a tunnel and then I saw a light.

In the morning of November 14, 2013, I was at my mother's house.

I have to specify that on November 13th, around 6:00 pm, I left my mother who was at the end of her life in a hospital. After telling her that I would be back next morning, I asked my brother to call me if her state got worse.

Between 6:00-6:45 am, I was lying awake in my mother's house. I started to feel a beneficial presence approaching rapidly. I sensed great serenity and joy, with a touch of solemn-ness.

At that moment, I perceived that this as a very singular experience, without making any link with my mother. A few minutes after this experience, I got a phone call from my brother at the hospital. He told me that mother had died. My brother fell asleep in the chair at the side of my mother's bed. He woke up at the moment she exhaled her last breath. He couldn't inform me about my mother's state, to allow me to come and be with her.

Strangely, on the morning of November 14th, at the hospital, I recalled my drowning at the age of 6 years. What I experienced at that moment came back to my memory.
