
I was on the front of a toboggan sled with my father behind me. The hill was icy. We headed for a large tree and couldn't steer away from it. I remember trying very hard to 'lean right' as he told me but it made no difference. My feet were under a rope and I couldn't jump or roll off. I ended up with a skull fracture, a broken tooth and nose, and lots of bruising. After that, I developed allergies, stomach problems, skin conditions, and scoliosis. I now know those conditions were due to anxiety and PTSD. Years of bodywork and other therapies with flashbacks, anxiety attacks and recreations have helped me understand what happened and why.

I have come to realize I left because I had psychic abilities that were not o.k. to share with my family and other people. I thought it was too hard to deny what I knew and who I was, so I decided to find a way to leave.

When I blacked out, I was bathed in white light. I had a feeling of joy and bliss. I saw a tunnel and heard voices telling me it was okay to go toward it. I could 'see' my distraught father trying to wake me and my mother screaming and running down the hill. I was torn between the bliss and knowing what my death would do to my parents. I chose to return.

I awoke as my father was carrying me up the hill. I was in and out of consciousness on the drive and in the hospital. I remember waking with a bright light overhead as I was getting stitches. For a moment, I wondered if I was in THE light but the feeling wasn't the same.

For 35 years I was numbed out, the freeze part of the sympathetic response, and then began to have flashbacks. I still have some flashbacks periodically, though fewer now than 20 years ago. I am super conscious and sensitive and gaining my psychic and intuitive gifts back also.
