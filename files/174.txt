

My brother Bruce, my dog, Candy and I went in my truck across town to teach a friend how to cut roof rafters. The temperatures were very warm and projected to top out near 100ºF that day. I got about 2 hours into the instructions when I felt very weak and had a bad headache. I noticed that I had stopped sweating. My body was on fire. I knew instantly that I had to cool off immediately. I told my friend I would meet him at a local brewery later where I could instruct him further while we cooled off. My brother and I drove to my daughter's apartment to drop off my dog. When we got there, I couldn't remember their door code, yet I go there at least 6 times a week. I called her, found out she was home, got the code and went to her apartment. She immediately noticed that I didn't look good. She gave me a large glass of ice water and told me to lay down. I downed the water and told her I'd be o.k. Then I drove to the brewery with my brother.

We met my friend at the brewery 2 miles away. I continued the rafter/roof cutting instructions while enjoying a beer.

About a half hour after arriving, I experienced the most intense leg cramps and told my friend I had to go out to walk it off. I did so, but knew the cramps would most likely reoccur. So, I told my brother I needed him to drive me home. He suggested we go to his house where I could lay on his guest bed while he rehydrated and monitored me. As soon as I laid on his guest bed, I told him I needed to get to the emergency room immediately because I knew I was in serious trouble and if I didn't get immediate medical attention that I was going to die.

Bruce helped me into the front passenger seat of his truck then proceeded to the hospital about 7 miles away. I told Bruce that I felt like I was going to die, possibly before we got to the hospital. He called 911 to notify the hospital of my problem and that we were on the way. I called my adult kids and told them to meet us there, that I didn't think I was going to live through this event, and I wanted to say goodbye. After that call, I decided to record my Last Will & Testament on my cellphone.

We got to the hospital. An orderly came with a wheelchair to the truck, but I was unable to move my legs because I had no feeling in them at all. Bruce helped me get into the wheelchair. Then they whisked me into triage, and then right into the emergency room.

I was placed on a bed and immediately knew that this was the end.

I was not scared. I was very sad that my kids were going to be fatherless. I devoted my life to them and was looking forward to grandkids and watching my kids future. I knew everything that was being done and said by everyone in the room. My hearing became extremely acute as did my usually fair eyesight. The doctors and nurses continually asked me what I was feeling as they performed their jobs. I described every feeling and vision I was experiencing in detail. I felt my life slipping away multiple times. When that happened, I'd tell my family I love them and it was time to go. I visioned the room and my physical body shrinking as my spirit was rising from my body. I saw many deceased friends and relatives looking at me and telling me it's ok. I was brought back to life repeatedly by my family who begged me not to go.

This happened at least six times over a period of 3 hours, with each experience deeper than the last. I told my family that if I fall asleep, I will never wake up. They continued to tell me I cannot leave now and begged me to stay. They honestly are the reason I am still breathing.

I distinctly remember a vision, as my eyes were open, of clouds clearing. I saw a tree-lined forest road with my deceased relatives and friends pointing the way. This occurred as I watched my soul rising from my body while my family begged me to stay. I felt my body convulse a few times. I said, 'It's time for me to go. I love you all.' Then, I closed my eyes in complete comfort. It was the most peaceful feeling I had ever experienced in my life. I was unafraid of death and was now looking forward to an eternity of peace.

My family shook and squeezed me while begging me to stay with them. It worked. I am still here.

The doctors had already given me two bags of fluids and had just started a third bag. I lost all feeling and control of my entire body. An x-ray technician brought in a portable x-ray machine. He wanted to take pictures of my lungs. He asked me to bend forward but I felt like a pile of jell-o. I could not move any part of my body. I was able to talk slowly, see and hear, but had no feeling anywhere else whatsoever. The doctor noticed and performed some neurologic tests to determine if I was faking it. He realized that I was not and ordered an immediate CT scan of my head, believing I may have had an actual stroke or aneurism. The CT scan came back normal.

I finally regained body function/control after the third bag of IV fluids. As the last third of the bag flowed into me, I could actually feel life flowing back into my body. It was the same feeling I had when I had my own two units of blood re-infused into me after a major spinal surgery decades prior.

The doctors and nurses repeatedly asked if I remembered what happened, what day it was, where I was, what city I was in, etc. I answered correctly at all times. I was completely coherent at all times and non-delusional.


