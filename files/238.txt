

My experience happen when I was around 10 or 11 years old. Now I'm 39, but I just remembered this experiences a few years ago, since I had forgotten it. I was a weak child. I often missed school two or three days a month because of sickness.

On that day of my experience, I fell sick with a fever and sporadic headache. Usually, if the fever continuied for two or three days, my parents would drive me to the doctor. But this was just the first day of my fever. My parents needed to go to work, so they sent me to my grandma's house. My grandma's house is next door to my parent house.

In my grandma's room, it was noon and lunchtime. I woke up, but felt strange. I was floating. When I looked down, I could see myself sleeping in the bed. I felt confused and said in my mind, 'Hey that's me!' Then I felt myself going up. In front of my face is the roof of my grandma's house. I was a second away from hitting the roof, so I covered my head with my hand and close my eyes. Suddenly, I was through the roof and outside and over the house.

Since it was noon, the sun is shining on its peak. It should be so hot since Indonesia is a tropical country. I covered my eyes to avoid the sunlight, but I was not dazzled by the light. I even could see the sun shining in the sk, and it wasn't hot at all. I felt the sunlight was soft on my skin and eyes. I could see people around the house. I saw my parent's helper doing his job repairing vehicles, so I called his name. But he couldn't hear my voice. I yelled his name again, but still he can't hear me. It's so weird. Then I see my school classmates going home from school as she was passing my house. I call her name, but it's the same - she couldn't hear my voice. I felt so confused.

Suddenly, I felt pulled up. I was moving up very fast. I was moving through many, billions and billions a little shiny dots around me. I think they were stars. In a second, I stopped moving. I don't know if it's a place or not, because I don't have any words to describe this. A place always has dimensions such as width and height. But it's not. This was an unlimited place without borders or time.

There are no words to describe what I saw or how I felt because it's beyond what our physical body organ can feel, hear, smell, or touch. Is it a good view there? A colorful view? I can't describe, because it beyond the view. There, it's not a physical life. It's beyond our earthly existence.

Maybe the nearest feeling I can describe it as is the feeling of my soul. It's a very peaceful place and without time limitation, forever, and also without boundaries. But I made one mistake. I was looking backwards for a millisecond and felt so upset. I was looking at the place where I had left. It was a very little dot, very far away down there. Yes, I can see/feel the world I just left. It's like one grain of sand devided by million or billion, like it is microscopic or atomic size. And the place is so far away down there, it's a thousand or a million light years away.

I woke up with my grandma beside me, holding a plate for my lunch. She felt so happy when I woke up. Later, when my mom came home, my grandma told her that she was feeling afraid because she couldn't wake me up. She saw that my sleep was not usual. She couldn't recognize my breath, and my body was cold. My grandma was afraid to touch me, in case something happen where she could be blamed.

When I woke up, I felt so healthy. I didn't feel my fever or headache anymore. I even ran back to my house, feeling so fresh with new energy like I was never sick. But starting from that day, if I got sick, my grandma doesn't want to take care of me anymore. So my mom will take her leave from the office, to care for me if I got sick after that.

This experience is just forgotten for many years, from one day after that happening. Maybe since I was a child, I didn't give enough attention to this until I grew up. I just remembered all that experience a few years ago, part by part until I have a complete imagery right now.

According to the experience, I see that our world is just a very low grade of life. Our physical world is a shape of a raw life. Physical human body is so limited. Our body organ is useful for our physical life, but comparing with the life over there, this is just nothing. Yes, our life is nothing, it just a little dot far, far away. The real life over there is a life for our soul, happiness and peaceful. I even can't describe with a word, because no eyes can see it, no skin can touch it, and no nose can smell it. It is beyond our physical body. There, it is a life without dimension or time and is limitless. It is beyond everything in our earthly life.


