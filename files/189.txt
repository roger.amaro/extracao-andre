

I had an ICD (Implantable cardioverter-defibrillator) unit installed around 11 a.m. By 4 p.m. the hospital nurse advised me that I would be discharged to go home. I was ready to go when the nurse notice bleeding on my t-shirt and also notice some swelling. The discharge was cancelled and I was quickly taken to Ward 31. There two nurses applied a pressure bandage over the incission area. Around 9:30 p.m. I felt the swelling moving over to my neck area, causing me to feel like it was choking. The night nurse called in a senior nurse who put some marking on my neck to check if the swelling was getting any worse. By 10:00 p.m. my night nurse was so concerned she called the doctor to check on the swelling.

The doctor came in at 11:30 p.m. and asked me to sit on the bed and he cut the pressure bandage off. The bleeding appeared to have stopped and after checking me out asked the nurse to apply another pressure bandage. At this point the doctor went away and the nurse called another nurse to assist. Just before the nurses applied the new pressure bandage, I started to feel fainting and called out to the nurses twice. They than helped me to lie down. I was gone by the time my head touched the pillows.

I saw that I was on top of my body, some 2 meters up on the back of the bed. I saw myself lying down motionless while the nurse applied a mask on my face. When she removed the mask, I saw white gas coming from the mask. There were a lot of people in the room. I counted 3 males and 4 females. Just before I fainted, there were only two nurses in the room. Everyone appeared to be doing something on me. One nurse had my left hand on her legs and was putting a needle on my left hand. Aother nurse had connected a drip tube on my right hand.

I felt very peaceful and with no pain. The sight of my physical body felt unnatural. While I knew it was me, I felt no association to the body. There appeared to be a lot of noise in the room but I was not able to make out what they were saying.

When I woke up, I saw the nurse with the mask who had called out, 'Code blue.' I saw the 3 doctors, with the one beside me who called out, 'Upgrade to code red.' Then the doctor by my bed said 'confirm code red.' The doctor next to me wanted to call in the brain specialist and told me that I may have some brain damage. Another doctor checked me out and confirmed there was no damage to my brain function. I have no idea what the codes meant but the staff did not want to discuss the issue with me. I asked the nurse later that night and I was told that I had stopped breathing.

My surgeon came in the morning and kept me in the hospital for another four nights.

I have tried to get some answers but have gotten nowhere. I left messages for the night nurse who looked after me to contact me and she has not returned my calls.



