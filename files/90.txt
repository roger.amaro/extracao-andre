

I had been in labor for a long time, so a decision was made to use high forceps to deliver my baby. I have been told that high forceps are no longer used because of the risk of injury to the baby. It was decided that I would need extra pain medication before this procedure could be done, so something was injected into my IV line that I am told is no longer used because it has caused some deaths. I don't remember the name of it, but I remember feeling that I was 'leaving.' I actually said 'good-bye.' I heard someone say that my heart had stopped and I heard someone call for a crash cart.

I was on my way out of the room through a corner where the ceiling met two walls. I reached out and touched the ceiling. Then I stopped and looked over my shoulder. I could see a nurse pounding on my chest and others working to deliver my son. I could see the top of his head 'crowning' and I knew I had to go back because my baby needed me. There was no way I could leave him. I knew he was going to have a hard life and that he would not get through it without me. I was instantly back 'in' myself. While I was 'out,' I felt no pain at all. As soon as I was back 'in,' the pain was intense. Everyone was very tense and grim-faced, but also seemed relieved. My son was born looking very healthy and strong. But within 24 hours, he developed dark bruises on his head and shoulders from the forceps in what was later recorded as a 'traumatic delivery.'

A labor and delivery nurse stopped by my room the next day and said, 'You know we nearly lost you yesterday, don't you?' Someone overheard her say that, and she was disciplined by a supervisor.

My son is 41 years old now. He had many developmental problems as a child such as not walking, talking or meeting other 'milestones' within normal ranges. His life has been extremely difficult with diagnoses of autism, severe depression, substance abuse, bipolar disorder and other developmental and mental health disorders. I don't know whether the difficult delivery caused these problems or whether they added to conditions that would have developed anyway; there is a history of mental illness in male members of my family going back generations.

But I do know that I nearly died and maybe actually DID die for a brief time and was revived. I do know, as I knew then, that my son needed me and that I had to come back for him.


